echo "optimizing all png files, recursively"

optipng $1 *.png
optipng $1 Home\ Decorator/Images/*.png
optipng $1 Home\ Decorator/Images/Bottom\ Bar/*.png
optipng $1 Home\ Decorator/Images/Brands/*.png
optipng $1 Home\ Decorator/Images/Color\ Picker/*.png
optipng $1 Home\ Decorator/Images/Rooms/*.png
optipng $1 Home\ Decorator/Images/Sidebar/*.png
optipng $1 Home\ Decorator/Images/Tabs/*.png
