//
//  Support.h
//  Paint Harmony
//
//  Created by Joel Teply on 3/31/14.
//
//

#ifndef Paint_Harmony_Support_h
#define Paint_Harmony_Support_h

#ifndef __IPHONE_4_0
#warning "This project uses features only available in iOS SDK 4.0 and later."
#endif

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <HomeAugmentation/HomeAugmentation.h>
#import "Controls.h"
#import "PainterUtility.h"
#endif

#define APP_NAME @"Paint Harmony"
#define PRO_URL @"https://itunes.apple.com/us/app/paint-harmony/id616218410?ls=1&mt=8"
#define CONTACT_EMAIL @"paint-harmony@cambriantech.com"
#define INITIAL_IMAGE @"shutterstock_13368892.jpg"
#define DATABASE_NAME @"ColorModel.sqlite"
#define INITIAL_COLOR_CATEGORY @"ColorSmart Premium Plus"

//#define HARD_CODED_COLOR_1 [UIColor colorWithRed:0.937254902 green:0.8784313725 blue:0.6705882353 alpha:1.0f] //tawney birch
//#define HARD_CODED_COLOR_2 [UIColor colorWithRed:0.6549019608 green:0.5294117647 blue:0.3490196078 alpha:1.0f] //caramel

#include <sys/sysctl.h>
static inline unsigned int countCores()
{
    size_t len;
    unsigned int ncpu;
    
    len = sizeof(ncpu);
    sysctlbyname ("hw.ncpu",&ncpu,&len,NULL,0);
    
    return ncpu;
}

#define DATABASE_NAME (PainterUtility.isLite ? @"ColorModelLite.sqlite" : @"ColorModel.sqlite")

#define HAS_CAMERA ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerSourceTypeCamera])

#define IS_RETINA ([[UIScreen mainScreen] scale] > 1.0)
#define IS_MULTICORE (countCores() > 1)
#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPOD   ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPod touch" ] )
#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )
#define IS_IPAD ( [[[ UIDevice currentDevice ] model] hasPrefix:@"iPad"] )

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define DEFAULT_COLOR UIColorFromRGB(0xc0fce0)

#define UICOLOR_SIDEBAR_ITEM UIColorFromRGB(0x555555)
#define UICOLOR_SIDEBAR_ITEM_SELECTED UIColorFromRGB(0x2b2c2f)
#define UICOLOR_SIDEBAR_ITEM_TEXT UIColorFromRGB(0x555555)

#define UICOLOR_SIDEBAR_HEADER_TEXT UIColorFromRGB(0xffffff)
#define UICOLOR_SIDEBAR_HEADER_TEXT_SHADOW UIColorFromRGB(0x555555)

#define UICOLOR_SIDEBAR_BG UIColorFromRGB(0xffffff)

#define DELETE_BUTTON_COLOR UIColorFromRGB(0x555555)

#endif
