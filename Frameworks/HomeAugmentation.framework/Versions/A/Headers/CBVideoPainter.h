//
//  CBVideoPainter.h
//  Cambrian
//
//  Created by Joel Teply on 1/17/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import "CBVideoDevice.h"
#import "CBImagePainter.h"

@interface CBVideoPainter : CBVideoDevice //<GPUImageTextureOutputDelegate>


@property (nonatomic, assign) BOOL showOutlines;
@property (nonatomic, assign) BOOL debugMode;
@property (nonatomic, assign) CGPoint paintPoint;
@property (nonatomic, retain) UIColor *paintColor;

- (void)captureCurrentState:(void (^)(CBImagePainterImage *imageWithMask))block;
- (void)captureCurrentState:(void (^)(CBImagePainterImage *imageWithMask))block
          constrainedToSize:(CGSize)maximumSize;

- (void) startRecording;
- (void) finishRecordingWithBlock:(void (^)(NSURL *file))block;
- (void) clearPaintPoints;

@end
