//
//  CBConstructs.h
//  Cambrian
//
//  Created by Joel Teply on 12/1/11.
//  Copyright (c) 2011 Digital Rising LLC. All rights reserved.
//

#ifndef Cambrian_CBTypes_h
#define Cambrian_CBTypes_h

typedef enum 
{
    CBAccuracyLow,
	CBAccuracyMedium,
    CBAccuracyHigh,
}  CBAccuracy;

typedef enum 
{
    CBFilterNone,
	CBFilterOverlay,
}  CBFilter;

typedef enum 
{
    CBColorSpaceHSV,
	CBColorSpaceRGBA,
    CBColorSpaceBW,
    CBColorSpaceLab,
}  CBColorSpace;


typedef struct _CBSample
{
    CGPoint origin;
    int radius;
    CGSize size;
    
    double  rgbMean[4];
    double  rgbDeviation[4];
    
    double  hsvMean[4];
    double  hsvDeviation[4];
    
    double  hsvMaskedMean[4];
    double  hsvMaskedDeviation[4];
    
    BOOL  hsvApplied[4];
    
    double  labMean[4];
    double  labDeviation[4];
    BOOL  labApplied[4];
}
CBSample;

typedef struct _CBMaskingParameters
{
    double hsvThresholds[4];
    BOOL applyHsvThresholds[4];
    
    double labThresholds[4];
    BOOL applyLabThresholds[4];

    BOOL applyEdges;
    BOOL showMasking;
}
CBMaskingParameters;

#endif
