//
//  TapScrollView.h
//  Wall Painter
//
//  Created by Joel Teply on 9/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TapScrollView : UIScrollView {
	
}

@property (assign, nonatomic) BOOL twoFingerScrolling;

@end
