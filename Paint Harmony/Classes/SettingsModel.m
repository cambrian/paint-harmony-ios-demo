//
//  SettingsModel.m
//  VirtualPainter
//
//  Created by Joel Teply on 11/13/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "SettingsModel.h"

@implementation SettingsModel

@dynamic initialVersion;
@dynamic matchPickerColors;
@dynamic hasSeenHarmonyDirections;

+ (SettingsModel *)getInstance {
    static SettingsModel *model = nil;
    if (model == nil) {
        
        model = [[SettingsModel alloc] init];
    }
    return model;
}

- (id) init
{
    self = [super init];
    
    if (!self.initialVersion) {
        NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
        [self setInitialVersion:version];
    }
    
    return self;
}

#define initialVersionKey @"initialVersion"
- (NSString *) initialVersion
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:initialVersionKey];
}

- (void) setInitialVersion:(NSString *)value
{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:initialVersionKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#define matchPickerColorsKey @"matchPickerColors"
- (BOOL) matchPickerColors
{
    return [self getBooleanForKey:matchPickerColorsKey usingDefault:YES];
}

- (void) setMatchPickerColors:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:matchPickerColorsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#define matchBrandKey @"matchBrand"
- (BOOL) matchBrand
{
    return [self getBooleanForKey:matchBrandKey usingDefault:YES];
}

- (void) setMatchBrand:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:matchBrandKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#define matchCategoryKey @"matchCategory"
- (BOOL) matchCategory
{
    return [self getBooleanForKey:matchCategoryKey usingDefault:NO];
}

- (void) setMatchCategory:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:matchCategoryKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#define hasSeenHarmonyDirectionsKey @"hasSeenHarmonyDirections"
- (BOOL) hasSeenHarmonyDirections
{
    return [self getBooleanForKey:hasSeenHarmonyDirectionsKey usingDefault:NO];
}

- (void) setHasSeenHarmonyDirections:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:hasSeenHarmonyDirectionsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#define showPinchZoomDirectionsKey @"showPinchZoomDirections"
- (BOOL) showPinchZoomDirections
{
    return [self getBooleanForKey:showPinchZoomDirectionsKey usingDefault:YES];
}

- (void) setShowPinchZoomDirections:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:showPinchZoomDirectionsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#define showScrollZoomDirectionsKey @"showScrollZoomDirections"
- (BOOL) showScrollZoomDirections
{
    return [self getBooleanForKey:showScrollZoomDirectionsKey usingDefault:YES];
}

- (void) setShowScrollZoomDirections:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:showScrollZoomDirectionsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#define hasSeenFavoritesDirectionsKey @"hasSeenFavoritesDirections"
- (BOOL) hasSeenFavoritesDirections
{
    return [self getBooleanForKey:hasSeenFavoritesDirectionsKey usingDefault:NO];
}

- (void) setHasSeenFavoritesDirections:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:hasSeenFavoritesDirectionsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#define hasSeenLayerDirectionsKey @"hasSeenLayerDirections"
- (BOOL) hasSeenLayerDirections
{
    return [self getBooleanForKey:hasSeenLayerDirectionsKey usingDefault:NO];
}

- (void) setHasSeenLayerDirections:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:hasSeenLayerDirectionsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#define hasSeenOverlayTutorialKey @"hasSeenOverlayTutorial"
- (BOOL) hasSeenOverlayTutorial
{
    return [self getBooleanForKey:hasSeenOverlayTutorialKey usingDefault:NO];
}

- (void) setHasSeenOverlayTutorial:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:hasSeenOverlayTutorialKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Key Access Methods

- (BOOL) getBooleanForKey:(NSString *)key usingDefault:(BOOL)defaultValue
{
    if ([self keyExists:key]) {
        return [[NSUserDefaults standardUserDefaults] boolForKey:key];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:defaultValue forKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return defaultValue;
    }
}

- (NSInteger) getIntegerForKey:(NSString *)key usingDefault:(NSInteger)defaultValue
{
    if ([self keyExists:key]) {
        return [[NSUserDefaults standardUserDefaults] integerForKey:key];
    } else {
        [[NSUserDefaults standardUserDefaults] setInteger:defaultValue forKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return defaultValue;
    }
}

- (BOOL)keyExists:(NSString *)key
{
    return NULL != [[NSUserDefaults standardUserDefaults] stringForKey:key];
}

@end
