//
//  SettingsModel.h
//  VirtualPainter
//
//  Created by Joel Teply on 11/13/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingsModel : NSObject

@property (assign, nonatomic) NSString *initialVersion;
@property (assign, nonatomic) BOOL matchPickerColors;
@property (assign, nonatomic) BOOL matchBrand;
@property (assign, nonatomic) BOOL matchCategory;

@property (assign, nonatomic) BOOL showPinchZoomDirections;
@property (assign, nonatomic) BOOL showScrollZoomDirections;
@property (assign, nonatomic) BOOL hasSeenHarmonyDirections;
@property (assign, nonatomic) BOOL hasSeenFavoritesDirections;
@property (assign, nonatomic) BOOL hasSeenLayerDirections;
@property (assign, nonatomic) BOOL hasSeenOverlayTutorial;

+ (SettingsModel *)getInstance;

@end
