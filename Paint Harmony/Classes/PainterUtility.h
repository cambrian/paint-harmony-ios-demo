//
//  PainterUtility.h
//  VirtualPainter
//
//  Created by Joel Teply on 11/7/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ColorChangedNotification @"ColorChangedNotification"
#define FavoritesChangedNotification @"FavoritesChangedNotification"

@class Brand, ColorCategory, Color;


@interface PainterUtility : NSObject

+ (UIImageView *)makeColorTexture:(UIImage *)texture withColor:(UIColor *)color withFrame:(CGRect)frame;

+ (UIImage *)makeColorTextureImage:(UIImage *)texture withColor:(UIColor *)color withFrame:(CGRect)frame;

+ (BOOL)isTouch:(UITouch *)touch withinView:(UIView *)view;

+ (NSArray *) allBrands;
+ (NSArray *) allColors;

+ (Color *)closestMatchForUIColor:(UIColor *)uiColor;
+ (Color *)closestMatchForUIColor:(UIColor *)uiColor brand:(Brand*)brand category:(ColorCategory*)category;

+ (Color *)closestMatchForUIColor:(UIColor *)uiColor
                     withinColors:(id<NSFastEnumeration>)colors
                  excludingColors:(NSArray *)colorsToExclude;

//+ (void)gravityForViews:(NSArray *)views isReversed:(BOOL)upIsDown ignoreView:(UIView *)ignoreView;

+ (void) saveImageToPhotoAlbum:(UIImage *)image;

+ (void) saveVideoToPhotoAlbum:(NSURL *)url;

+ (void)popupWithStatus:(NSString *)status queue:(dispatch_queue_t)queue block:(void (^)())block;

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

+ (NSMutableArray *) favorites;

+ (Color *) currentColor;
+ (UIColor *) currentUIColor;

+ (void) setFavoriteColor:(Color *)color isFavorite:(BOOL)isFavorite;

+ (void) setCurrentColor:(Color *)color
                 uiColor:(UIColor *)uiColor
                  sender:(id)sender;

+ (void) enableCheckbox:(UIButton *)checkbox enabled:(BOOL)enabled;

+ (BOOL) canMatchBrand:(Brand *)brand;
+ (BOOL) matchBrand:(Brand *)brand;

+ (BOOL) canMatchCategory:(ColorCategory *)category;
+ (BOOL) matchCategory:(ColorCategory *)category;

+ (NSString *)appStoreHTML;

+ (UIImageView *) setupTutorialStep:(UIView *)view
                              above:(BOOL)above
                           centered:(BOOL)centered;
+ (BOOL) isLite;
@end
