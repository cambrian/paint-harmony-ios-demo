//
//  PainterUtility.m
//  VirtualPainter
//
//  Created by Joel Teply on 11/7/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "PainterUtility.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "SVProgressHUD.h"
#import "Brand.h"
#import "ColorCategory.h"
#import "Color.h"
#import "Color+Extended.h"
#import "SettingsModel.h"

#define MIN_MATCHING_COLORS 200


static NSTimeInterval _lastFavoritesFetch = 0;
static NSMutableArray *_favorites;

static Color *_currentColor;
static UIColor *_currentUIColor;

@implementation PainterUtility

+ (BOOL) isLite {
    NSString *bundleID = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
    BOOL isLite = [bundleID isEqualToString:@"com.cambriantech.paintharmonylite"];
    return isLite;
}

+ (BOOL)isTouch:(UITouch *)touch withinView:(UIView *)view;
{
    CGPoint touchPoint = [touch locationInView:view];
    
    return touchPoint.x >= 0
    && touchPoint.y >= 0
    && touchPoint.x <= view.frame.size.width
    && touchPoint.y <= view.frame.size.height;
}

+ (UIImageView *)makeColorTexture:(UIImage *)texture withColor:(UIColor *)color withFrame:(CGRect)frame;
{
    UIImageView *view = [[UIImageView alloc] initWithFrame:frame];
    view.backgroundColor = color;
    
    if (texture) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            if (!view) {
                NSLog(@"Expired, no view or superiew");
                return;
            }
            
            UIImage *result = [self makeColorTextureImage:texture withColor:color withFrame:frame];
            
            if (!result) {
                NSLog(@"Expired");
                return;
            }
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (!view) return;
                view.image = result;
                view.contentMode = UIViewContentModeTopLeft;
                view.frame = frame;
            });
        });
    }
    
    return view;
}

+ (UIImage *)makeColorTextureImage:(UIImage *)texture withColor:(UIColor *)color withFrame:(CGRect)frame;
{
    CBImage *cbImage = [[CBImage alloc] initWithUIImage:texture];
    cbImage.canvasSize = IS_RETINA ? frame.size : CGSizeMake(frame.size.width * 2.0, frame.size.height * 2.0);
    
    [cbImage appendNewLayer];
    cbImage.topLayer.filter = CBFilterColor;
    [cbImage.topLayer fillWithColor:color];
    
    return cbImage.image;
}

+ (NSArray *) allBrands;
{
    NSArray *brands = [[CBCoreData sharedInstance] getRecordsForClass:[Brand class] predicate:nil sortedBy:nil context:nil];
    
    NSMutableArray *filteredBrands = [NSMutableArray array];
    for (Brand *brand in brands) {
        if ([brand.categories count]) {
            [filteredBrands addObject:brand];
        }
    }
    
    return filteredBrands;
}

+ (NSArray *)allColors;
{
    return [[CBCoreData sharedInstance] getRecordsForClass:[Color class] predicate:nil sortedBy:nil context:nil];
}

+ (Color *)closestMatchForUIColor:(UIColor *)uiColor;
{
    NSArray *lotsOfColors = [NSArray array];
    int delta = 8;
    while ([lotsOfColors count] == 0 && delta < 100) {
        lotsOfColors = [self colorsMatchingUIColor:uiColor withinDelta:delta];
        delta = delta * 2;
    }
    return [self closestMatchForUIColor:uiColor withinColors:lotsOfColors excludingColors:NULL];
}

+ (Color *)closestMatchForUIColor:(UIColor *)uiColor brand:(Brand*)brand category:(ColorCategory*)category;
{
    if (!brand) {
        return [self closestMatchForUIColor:uiColor];
    }
    else if (category) {
        return [self closestMatchForUIColor:uiColor withinColors:category.colors excludingColors:NULL];
    }
    else {
        NSMutableArray * colors  = [NSMutableArray array];
        
        for (ColorCategory *category in brand.categories) {
            [colors addObjectsFromArray:category.colors.allObjects];
        }
        
        return [self closestMatchForUIColor:uiColor withinColors:colors excludingColors:NULL];
    }
}

+ (NSArray *)colorsMatchingUIColor:(UIColor *)uiColor withinDelta:(int)delta
{
    CGFloat r, g, b;
    [uiColor getRed:&r green:&g blue:&b alpha:nil];
    
    int red = 255.0 * r;
    if (red - delta < 0) red = delta;
    else if (red + delta > 255) red = 255 - delta;
    
    int green = 255.0 * g;
    if (green - delta < 0) green = delta;
    else if (green + delta > 255) green = 255 - delta;
    
    int blue = 255.0 * b;
    if (blue - delta < 0) blue = delta;
    else if (blue + delta > 255) blue = 255 - delta;
    
    
    NSMutableString *query = [NSMutableString stringWithString:@"(red > %i && red < %i\
                              && green > %i && green < %i\
                              && blue > %i && blue < %i)"];
        
    NSPredicate *predicate = [NSPredicate predicateWithFormat:query,
                              red - delta, red + delta,
                              green - delta, green + delta,
                              blue - delta, blue + delta];
    
    
    return [[CBCoreData sharedInstance] getRecordsForClass:[Color class] predicate:predicate sortedBy:nil context:nil];
}

+ (Color *)closestMatchForUIColor:(UIColor *)uiColor
                     withinColors:(id<NSFastEnumeration>)colors
                  excludingColors:(NSArray *)colorsToExclude;
{
    double bestDistance = DBL_MAX;
    Color *bestColor;
    for (Color *color in colors) {
        if (colorsToExclude && [colorsToExclude containsObject:color]) {
            continue;
        }
        double distance = [color distanceFromUIColor:uiColor];
        if (distance < bestDistance) {
            bestDistance = distance;
            bestColor = color;
        }
    }
    
    return bestColor;
}

+ (void) saveImageToPhotoAlbum:(UIImage *)image;
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeImageToSavedPhotosAlbum:[image CGImage]
                              orientation:(ALAssetOrientation)[image imageOrientation]
                          completionBlock:^(NSURL *assetURL, NSError *error){
                              if (!error) NSLog(@"Saved image to photo album");
                              else [self promptForPermissionsIssue];
                          }];
}

+ (void) saveVideoToPhotoAlbum:(NSURL *)url;
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeVideoAtPathToSavedPhotosAlbum:url
                                completionBlock:^(NSURL *assetURL, NSError *error){
                                    if (!error) NSLog(@"Saved video to photo album");
                                    else [self promptForPermissionsIssue];
                                    [[NSFileManager defaultManager] removeItemAtURL:url error:nil];
                                }];
}

+ (void) promptForPermissionsIssue {
    if ([ALAssetsLibrary authorizationStatus] != ALAuthorizationStatusAuthorized) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Access to Photos not Granted"
                                                        message:@"This application cannot access your photo album. To enable it, go to:\n\nSettings->Privacy->Photos"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

+ (void)popupWithStatus:(NSString *)status queue:(dispatch_queue_t)queue block:(void (^)())block;
{
    [SVProgressHUD showWithStatus:status];
    NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
    dispatch_async(queue, ^{
        
        if (block) block();
        
        NSTimeInterval timeTaken = [NSDate timeIntervalSinceReferenceDate] - start;
        double delayInSeconds = (timeTaken < 1.0) ? 1.0 - timeTaken : 0.01;
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [SVProgressHUD dismiss];
        });
    });
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    if (&NSURLIsExcludedFromBackupKey == nil) { // iOS < 5.0.1
        return false;
    }
    else { // iOS >= 5.1
        NSError *error = nil;
        [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
        return error == nil;
    }
}

+ (NSMutableArray *) favorites
{
    NSTimeInterval favoritesAge = [NSDate timeIntervalSinceReferenceDate] - _lastFavoritesFetch;
    if (!_favorites || favoritesAge > 30) {
        NSPredicate *query = [NSPredicate predicateWithFormat:@"favorite==1"];
        _favorites = [NSMutableArray  arrayWithArray:[[CBCoreData sharedInstance] getRecordsForClass:[Color class] predicate:query sortedBy:nil context:nil]];
        _lastFavoritesFetch = [NSDate timeIntervalSinceReferenceDate];
    }
    return _favorites;
}

+ (Color *) currentColor;
{
    return _currentColor;
}

+ (UIColor *) currentUIColor;
{
    return _currentUIColor;
}

+ (void) setCurrentColor:(Color *)color
                  uiColor:(UIColor *)uiColor
                   sender:(id)sender;
{
    if (!color) return;
    
    _currentColor = color;
    _currentUIColor = uiColor;
    
    //notification
    [[NSNotificationCenter defaultCenter] postNotificationName:ColorChangedNotification
                                                        object:sender
                                                      userInfo:nil];
    
}

+ (void)setFavoriteColor:(Color *)color isFavorite:(BOOL)isFavorite;
{
    color.favorite = [NSNumber numberWithBool:isFavorite];
    [[[CBCoreData sharedInstance] managedObjectContext] save:nil];
    
    //have to do this, database delays
    if (isFavorite) {
        [_favorites addObject:color];
    } else {
        [_favorites removeObject:color];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:FavoritesChangedNotification
                                                        object:nil
                                                      userInfo:nil];

}

+ (void) enableCheckbox:(UIButton *)checkbox enabled:(BOOL)enabled
{
    checkbox.enabled = enabled;
    checkbox.alpha = checkbox.enabled ? 1.0 : 0.5;
}

+ (BOOL) canMatchBrand:(Brand *)brand
{
    int totalColors = 0;
    for (ColorCategory *category in brand.categories) {
        totalColors += [category.colors count];
    }
    return totalColors > MIN_MATCHING_COLORS;
}

+ (BOOL) matchBrand:(Brand *)brand
{
    BOOL matchColors = [[SettingsModel getInstance] matchPickerColors];
    BOOL matchBrand = [[SettingsModel getInstance] matchBrand];
    if (matchColors && matchBrand) {
        return [self canMatchBrand:brand];
    }
    return NO;
}

+ (BOOL) canMatchCategory:(ColorCategory *)category
{
    return [category.colors count] > MIN_MATCHING_COLORS;
}

+ (BOOL) matchCategory:(ColorCategory *)category
{
    BOOL matchColors = [[SettingsModel getInstance] matchPickerColors];
    BOOL matchBrand = [[SettingsModel getInstance] matchBrand];
    BOOL matchCategory = [[SettingsModel getInstance] matchCategory];
    if (matchColors && matchBrand && matchCategory) {
        return [self canMatchCategory:category];
    }
    return NO;
}

+ (NSString *)appStoreHTML
{
    NSMutableString * result = [NSMutableString stringWithFormat:@"<i>Sent from %@ for ", APP_NAME];
    if (IS_IPAD) {
        [result appendString:@"iPad"];
    } else {
        [result appendString:@"iPhone"];
    }
    [result appendString:@"</i><br />"];
    [result appendFormat:@"<a href=\"%@\">Click here for more</a>", PRO_URL];
    return result;
}

+ (UIImageView *) setupTutorialStep:(UIView *)view
                              above:(BOOL)above
                           centered:(BOOL)centered;
{    
    float angle = above ? 0 : M_PI;
    
    UIImage *arrowImage = [UIImage imageNamed:@"tutorial-arrow-up.png"];
    
    float xOffset = centered ? view.frame.size.width / 2 - arrowImage.size.width / 2 : -5;
    float yOffset = above ?  - arrowImage.size.height - 5 : view.frame.size.height + 5;
    
    CGPoint arrowPosition = CGPointMake(view.frame.origin.x + xOffset, view.frame.origin.y + yOffset);
    
    UIImageView *arrow = [[UIImageView alloc] initWithImage:arrowImage];
    arrow.frame = CGRectSetOrigin(arrowPosition, arrow.frame);
    if (angle) arrow.transform = CGAffineTransformMakeRotation(angle);
    [view.superview insertSubview:arrow belowSubview:view];
    
    arrow.hidden = view.hidden;
    return arrow;
}

@end
