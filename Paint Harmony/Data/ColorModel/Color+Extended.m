//
//  Color+Extended.m
//  ColorParser
//
//  Created by Joel Teply on 1/25/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import "Color+Extended.h"

@implementation Color (Extended)

@dynamic uiColor;

- (UIColor *)uiColor;
{
    return [UIColor colorWithRed:[self.red doubleValue]/255.0f
                           green:[self.green doubleValue]/255.0f
                            blue:[self.blue doubleValue]/255.0f
                           alpha:1.0];
}

- (void) setUiColor:(UIColor *)uiColor;
{
    const CGFloat *components = uiColor ? CGColorGetComponents(uiColor.CGColor)
        : CGColorGetComponents([UIColor clearColor].CGColor);
    
    self.red = [NSNumber numberWithFloat:components[0] * 255.0f];
    self.green = [NSNumber numberWithFloat:components[1] * 255.0f];
    self.blue = [NSNumber numberWithFloat:components[2] * 255.0f];
}

#ifdef HOMEAUGMENTATION
- (double) distanceFromUIColor:(UIColor *)color;
{
    return [CBColoring distance:color fromColor:self.uiColor];
}

+ (Color *) colorForLayer:(CBLayer *)layer {
    NSString *objectID = [layer.userData objectForKey:@"object_id"];
    if (objectID) {
        NSURL *objectURL = [NSURL URLWithString:objectID];
        Color *color = [[CBCoreData sharedInstance] getObjectWithUrl:objectURL];
        return color;
    }
    return nil;
}

- (void)setLayerColorInfo:(CBLayer *)layer {
    NSURL *url = self.objectID.URIRepresentation;
    NSString *value = [url absoluteString];
    //NSLog(@"setLayerColorInfo:%@, %@", self.name, value);
    [layer.userData setObject:value forKey:@"object_id"];
    layer.fillColor = self.uiColor;
}
#endif
@end
