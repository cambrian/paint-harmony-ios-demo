//
//  Color+Extended.h
//  ColorParser
//
//  Created by Joel Teply on 1/25/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import "Color.h"

@interface Color (Extended)

@property (nonatomic, retain) UIColor *uiColor;

#ifdef HOMEAUGMENTATION
- (double) distanceFromUIColor:(UIColor *)color;
- (void)setLayerColorInfo:(CBLayer *)layer;
+ (Color *) colorForLayer:(CBLayer *)layer;
#endif

@end
