//
//  SubViewController.h
//  VirtualPainter
//
//  Created by Joel Teply on 11/20/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "BaseViewController.h"

@interface SubViewController : BaseViewController

@property (retain, nonatomic) UIViewController *parent;

@property (assign, nonatomic) BOOL isPresentingModal;

@end
