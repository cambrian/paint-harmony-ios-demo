//
//  SubViewController.m
//  VirtualPainter
//
//  Created by Joel Teply on 11/20/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "SubViewController.h"

@interface SubViewController ()


@end

@implementation SubViewController

@synthesize parent = _parent;
@synthesize isPresentingModal = _isPresentingModal;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (!self.isPresentingModal) {
        [_parent viewWillAppear:animated];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if (!self.isPresentingModal) {
        [_parent viewDidAppear:animated];
    }
}

@end
