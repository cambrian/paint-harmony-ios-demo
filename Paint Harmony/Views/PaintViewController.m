//
//  FirstViewController.m
//  Wall Painter
//
//  Created by Joel Teply on 11/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "PaintViewController.h"
#import "ColorTabsViewController.h"
#import "Color+Extended.h"
#import "ColorCategory.h"
#import "Brand.h"
#import "SVProgressHUD.h"
#import "HarmonyViewController.h"
#import "PainterUtility.h"
#import "ColorViewController.h"
#import "PainterUtility.h"
#import "UAImagePickerController.h"
#import "SettingsModel.h"

#define FAVORITES_ALERT @"Favorites"
#define SAVE_CHANGES_ALERT @"Save Changes"
#define ZOOMING_INSTRUCTIONS @"To zoom the image, use the pinch gesture"
#define SCROLLING_INSTRUCTIONS @"To scroll the image, swipe two fingers.\nOne finger will use the current tool."
#define RECTANGLE_INSTRUCTIONS @"Drag each corner and then click the green check to commit or red X to ignore."

@interface PaintViewController() {
    
@private
    dispatch_source_t _still_mask_timer;
    
    UIPopoverController *_popover;
    BOOL _isFirstOpen;
    BOOL _layoutCompleted;
}

@end

@implementation PaintViewController

@synthesize colorNameButton;
@synthesize toolbar;
@synthesize currentColorView;
@synthesize colorTabs;
@synthesize unsavedWork = _unsavedWork;

@synthesize camera = _camera;


//#define LOAD_TEST @"project_1C9B273A-6682-46F2-86BC-56DC8E228582"
#define SAVE_TEST @"test"

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Try", @"Try");
        self.tabBarItem.image = [UIImage imageNamed:@"first"];
        
        self.camera = [[UAImagePickerController alloc] init];
        self.camera.delegate = self;
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.hideStatusBar) {
        [self.navigationController setNavigationBarHidden:YES];
    }
    
    __weak PaintViewController *weakSelf = self;
    
    self.sideDrawer = [[SideBarViewController alloc] initWithNibName:nil
                                                              bundle:nil];
    self.sideDrawer.delegate = self;
    self.sideDrawer.view.frame = CGRectSetX(0, self.sideDrawer.view.frame);
    self.sideDrawer.view.frame = CGRectSetH(self.view.frame.size.height, self.sideDrawer.view.frame);
    [self.view insertSubview:self.sideDrawer.view atIndex:0];
    self.sideDrawer.view.hidden = YES;
    
    self.drawer = [[CBDrawerController alloc] initWithDelegate:self
                                                   primaryView:self.primaryView
                                                   sidebarView:self.sideDrawer.view
                   shadowImage:[UIImage imageNamed:@"vertical_shadow.png"]];
    
    
    
    self.imagePainter.maxHistorySize = IS_RETINA ? 10 : 5;
    
    //customize these depending on interface and desires
    self.imagePainter.smartBrushEnabled = YES;
    self.imagePainter.autoBrushSizeEnabled = YES;
    self.imagePainter.brushTapFillEnabled = YES;
    
    self.instructionsView.hidden = YES;
    self.rectangleToolOptions.hidden = YES;
    
    [self.imagePainter setBusyLoadingBlock:^(BOOL completed) {
        if (completed) {
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD showWithStatus:@"Processing"];
        }
    }];
    
    [self.imagePainter setHistoryChangedBlock:^{
        [weakSelf updateTools];
        if (weakSelf.imagePainter.canStepBackward) {
            weakSelf.unsavedWork = YES;
        }
    }];
    
    [self.imagePainter setShouldStartToolBlock:^BOOL(ToolMode toolMode) {
        return YES;
    }];
    
    [self.imagePainter setStartedToolBlock:^(ToolMode toolMode) {
        if (toolMode == ToolModeRectangle) {
            [weakSelf showRectangleOptions:YES];
        }
    }];
    
    [self.imagePainter setFinishedToolBlock:^(ToolMode toolMode) {
        if (toolMode == ToolModeRectangle) {
            [weakSelf showRectangleOptions:NO];
        }
    }];
    
    [self.imagePainter setScrolledContentsBlock:^{
        //self.colorHighlighter.hidden = YES;
    }];
    
    [self.imagePainter setZoomingCompletedBlock:^{
        [weakSelf updateInstructions];
    }];
    
    self.imagePainter.paintColor = [UIColor redColor];
    
    NSString *initialFileName = INITIAL_IMAGE;
    
    //initialFileName = @"kitchen-1.png";
    //initialFileName = @"hall-1.png";
    //initialFileName = @"our_house.jpg";
    //initialFileName = @"neigh.jpg";
    //initialFileName = @"living-room-5.jpg";
    //initialFileName = @"dogs.jpg";
    //initialFileName = @"dining-2.png";
    //initialFileName = @"tweeters-room.png";
    //initialFileName = @"spalding-kitchen-1.png";
    //initialFileName = @"spalding-kitchen-2.png";
    //initialFileName = @"reza.png";
    //initialFileName = @"steves.jpg";
    
    //initialFileName = @"kids-1.png";
    //initialFileName = @"storefront.png";
    
    //initialFileName = @"living-4.png";
    
    UIImage *image = [UIImage imageNamed:initialFileName];
    [self loadImage:image hasAlphaMasking:NO];
    
    //Color Tabs
    self.colorTabs = [[ColorTabsViewController alloc] initWithNibName:nil bundle:nil];
    self.colorTabs.delegate = self;
    
    [self showPaintTools:false];

    [self.primaryView addSubview:self.colorTabs.view];
    
    ColorCategory *category = [[CBCoreData sharedInstance] getFirstRecordForClass:[ColorCategory class] predicate:[NSPredicate predicateWithFormat:@"name==%@", INITIAL_COLOR_CATEGORY]];
    
    self.colorTabs.colorCategory = category;
    
    [self pickRandomColor];
    
    [self colorChangedNotification:self];
    
    [self createLayersMenus];
    
    self.overlayTutorialView.hidden = [[SettingsModel getInstance] hasSeenOverlayTutorial];
    
#ifdef LOAD_TEST
    [self loadTest:LOAD_TEST];
#endif
    
}

- (void) saveAndLoadTest:(NSString *)projectID
{
    [self loadTest:projectID];
    [self saveTest:projectID];
}

- (void) loadTest:(NSString *)projectID
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    NSString *projectPath = [basePath stringByAppendingPathComponent:projectID];

    CBImagePainterImage *image = [[CBImagePainterImage alloc] initWithPath:projectPath];
    self.imagePainter.stillImage = image;
}

- (void) saveTest:(NSString *)projectID
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    NSString *projectPath = [basePath stringByAppendingPathComponent:projectID];
    [self.imagePainter.stillImage saveToDirectory:projectPath];
}


- (void) setDefaultToolMode {
    self.imagePainter.toolMode = ToolModePaintbrush;
    [self updateTools];
}

- (void) showInstructions:(BOOL)show text:(NSString *)text
{
    if (show) {
        float openY = CGRectGetMaxY(self.colorTabs.closedFrame) - 5;
        self.instructionsView.frame = CGRectSetY(openY, self.instructionsView.frame);
    }
    if (text) self.instructionsLabel.text = text;
    self.instructionsView.hidden = !show;
}

- (void)updateInstructions
{
    if (self.imagePainter.zoomScale == 1) {
        [self showInstructions:[[SettingsModel getInstance] showPinchZoomDirections]
                          text:ZOOMING_INSTRUCTIONS];
    } else {
        [self showInstructions:[[SettingsModel getInstance] showScrollZoomDirections]
                          text:SCROLLING_INSTRUCTIONS];
    }
}

- (IBAction) dismissInstructions:(id)sender {
    if (self.imagePainter.zoomScale == 1) {
        [[SettingsModel getInstance] setShowPinchZoomDirections:NO];
    } else {
        [[SettingsModel getInstance] setShowScrollZoomDirections:NO];
    }
    self.instructionsView.hidden = YES;
}

- (IBAction)commitRectangleClicked:(id)sender {
    [self.imagePainter commitChanges];
}

- (IBAction)decommitRectangleClicked:(id)sender
{
    [self.imagePainter decommitChanges];
}

- (IBAction)tutorialDismissed:(id)sender {
    [[SettingsModel getInstance] setHasSeenOverlayTutorial:YES];
    self.overlayTutorialView.hidden = YES;
}

- (void) showRectangleOptions:(BOOL)show
{
    if (show) {
        float openY = CGRectGetMaxY(self.colorTabs.closedFrame) - 5;
        self.rectangleToolOptions.frame = CGRectSetY(openY, self.rectangleToolOptions.frame);
    }
    self.rectangleToolInstructions.text = RECTANGLE_INSTRUCTIONS;
    self.rectangleToolOptions.hidden = !show;
}

- (void)tabOpened:(ColorTab)tab view:(UIView *)view;
{

}

- (void)tabClosed:(ColorTab)tab view:(UIView *)view;
{
    
}

- (void) pickRandomColor
{
    NSArray *allColors = [PainterUtility allColors];
    
    if (![allColors count]) return;
    
    UIColor *initialImageWallColor = UIColorFromRGB(0x9c896e);
    Color *initialColor = NULL;
    
    srand(time(NULL));
    
    for (int i=0; i<20; i++) {
        int randomIndex = rand() % [allColors count];
        
        Color *testColor = [allColors objectAtIndex:randomIndex];
        CGFloat hue, saturation, brightness;
        [testColor.uiColor getHue:&hue saturation:&saturation brightness:&brightness alpha:NULL];
        double testSaturation = saturation;
        double lastSaturation = 0;
        double wallDistance = [CBColoring distance:testColor.uiColor fromColor:initialImageWallColor];
        
        if (initialColor) {
            [initialColor.uiColor getHue:&hue saturation:&saturation brightness:&brightness alpha:NULL];
            lastSaturation = saturation;
        }
        
        if (!lastSaturation || (testSaturation > lastSaturation && wallDistance > 0.2)) {
            initialColor = testColor;
        }
    }
    [self SelectColor_PickedColor:initialColor uiColor:initialColor.uiColor];
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if (_layoutCompleted) return;
    _layoutCompleted = YES;
    
    self.colorTabs.view.frame = CGRectSetY(0, self.colorTabs.view.frame);
    

    self.colorNameView.frame = CGRectSetY(self.toolbar.frame.origin.y
                                           - self.colorNameView.frame.size.height - 5, self.colorNameView.frame);

    
    self.paintTools.frame = CGRectSetY(self.toolbar.frame.origin.y
                                       - self.paintTools.frame.size.height - 10, self.paintTools.frame);
    
    //tile bg
    UIImage *bgImage = [UIImage imageNamed:@"bg.png"];
    for (int x=0; x<self.primaryView.frame.size.width; x+= bgImage.size.width) {
        for (int y=0; y<self.primaryView.frame.size.height; y+= bgImage.size.height) {
            UIImageView *bgImageView = [[UIImageView alloc] initWithImage:bgImage];
            bgImageView.frame = CGRectSetOrigin(CGPointMake(x, y), bgImageView.frame);
            [self.primaryView insertSubview:bgImageView atIndex:0];
        }
    }

    float colorTabsMaxY = 45;//calc messed up
    self.imagePainter.frame = CGRectSetY(colorTabsMaxY, self.imagePainter.frame);
    
    float scrollHeight = CGRectGetMinY(self.colorNameView.frame) - colorTabsMaxY;
    self.imagePainter.frame = CGRectSetH(scrollHeight,
                                       self.imagePainter.frame);
}

- (void)viewDidUnload
{
    [self setCurrentColorView:nil];
    [self setToolbar:nil];
    [self setColorNameButton:nil];
    [self setPaintTools:nil];
    [self setPaintToolbarButton:nil];
    [self setUndoButton:nil];
    [self setCameraButton:nil];
    [self setSaveButton:nil];
    [self setPrimaryView:nil];
    [self setColorNameView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateInstructions];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _isFirstOpen = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark -
#pragma mark Painting

- (void) SelectColor_PickedColor:(Color *)color uiColor:(UIColor*)uiColor;
{
    ColorCategory *category = self.colorTabs.colorCategory;
    if (color) {
        category = color.category;
    }
    self.colorTabs.colorCategory = category;
    //Brand *brand = category ? category.brand : NULL;
    
    [PainterUtility setCurrentColor:color uiColor:uiColor sender:self];
//    [PainterUtility setCurrentColor:color uiColor:uiColor sender:self findMatch:YES brand:brand category:category];
}

- (void)colorChangedNotification:(id)sender
{
    Color *color = [PainterUtility currentColor];
    UIColor *uiColor = [PainterUtility currentUIColor];
    if (color) {
        self.colorNameButton.hidden = NO;
        [self.colorNameButton setTitle:[NSString stringWithFormat:@"%@ - %@", color.category.brand.name, color.name] forState:UIControlStateNormal];
    } else {
        self.colorNameButton.hidden = YES;
    }
    
    if (self.imagePainter.layerCount == 2) {
#ifdef HARD_CODED_COLOR_1
    uiColor = HARD_CODED_COLOR_1;
#endif
    } else if (self.imagePainter.layerCount == 3) {
#ifdef HARD_CODED_COLOR_2
        uiColor = HARD_CODED_COLOR_2;
#endif
    }

    self.currentColorView.backgroundColor = uiColor;
    self.imagePainter.paintColor = uiColor;
    [color setLayerColorInfo:self.imagePainter.editLayer];
    
    if (self.imagePainter.toolMode == ToolModeEraser) {
        [self setDefaultToolMode];
    }
}

- (void)takePhoto:(BOOL)animate;
{
    if (HAS_CAMERA) {
        self.camera.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.camera.allowsEditing = NO;
        self.camera.showsCameraControls = YES;
        [self presentModalViewController:self.camera animated:animate];
    } else {
        [self getPhoto:animate];
    }
}

- (void)getPhoto:(BOOL)animate;
{
    UIImagePickerController *libraryPicker = [[UIImagePickerController alloc] init];
    libraryPicker.delegate = self;
    libraryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    if (IS_IPAD) {
        _popover = [[UIPopoverController alloc] initWithContentViewController:libraryPicker];
        _popover.delegate = self;
        CGRect centerRect = CGRectCenter(CGRectMake(1, 1, 1, 1), self.view.frame);
        [_popover presentPopoverFromRect:centerRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:animate];
    } else {
        [self presentModalViewController:libraryPicker animated:animate];
    }
}

#pragma mark -
#pragma mark UIImagePicker, UINavigationController Protocols

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // No good with the edited image (if you allowed editing)
    //m_image.image = [info objectForKey:UIImagePickerControllerEditedImage];
    // AND no good with the original image
    //m_image.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    // AND no doing some other kind of assignment
    //UIImage *myImage = [info objectForKey:UIImagePickerControllerEditedImage];
    [picker dismissModalViewControllerAnimated:YES];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [self loadImage:image hasAlphaMasking:NO];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    if (IS_IPAD && picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        [_popover dismissPopoverAnimated:YES];
    } else {
        [picker dismissModalViewControllerAnimated:YES];
    }
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated;
{
    
}

- (void) updateTools
{
    UIImage *selectedImage;
    UIImage *unselectedImage;
    switch (self.imagePainter.toolMode) {
        case ToolModeFill:
            unselectedImage = [UIImage imageNamed:@"bucket_button.png"];
            selectedImage = [UIImage imageNamed:@"bucket_button_on.png"];
            break;
        case ToolModeRectangle:
            unselectedImage = [UIImage imageNamed:@"rect_button.png"];
            selectedImage = [UIImage imageNamed:@"rect_button_on.png"];
            break;
        case ToolModePaintbrush:
            unselectedImage = [UIImage imageNamed:@"paint_button.png"];
            selectedImage = [UIImage imageNamed:@"paint_button_on.png"];
            break;
        case ToolModeEraser:
            unselectedImage = [UIImage imageNamed:@"eraser_button.png"];
            selectedImage = [UIImage imageNamed:@"eraser_button_on.png"];
            break;
        default:
            break;
    }
    
    [self.paintToolbarButton setImage:unselectedImage forState:UIControlStateNormal];
    [self.paintToolbarButton setImage:selectedImage forState:UIControlStateHighlighted];
    [self.paintToolbarButton setImage:selectedImage forState:UIControlStateSelected];
    
    self.undoButton.enabled = self.imagePainter.canStepBackward;
}

- (void) showPaintTools:(BOOL)show
{
    [self closeAllOverlays:self.paintTools];
    
    double closePosition = self.toolbar.frame.origin.y;
    double openPosition = self.toolbar.frame.origin.y - self.paintTools.frame.size.height - 5;
    
    BOOL isOpen = !self.paintTools.hidden;
    
    if (isOpen == show) return;
    
    self.paintTools.frame = CGRectSetX(self.paintToolbarButton.frame.origin.x, self.paintTools.frame);
    
    self.paintToolbarButton.selected = show;
    self.paintTools.frame = CGRectSetY(show ? closePosition : openPosition, self.paintTools.frame);
    self.paintTools.hidden = NO;
    
    [UIView animateWithDuration:0.1 animations:^{
        self.paintTools.frame = CGRectSetY(show ? openPosition : closePosition, self.paintTools.frame);
    } completion:^(BOOL finished) {
        self.paintTools.hidden = !show;
    }];
}

- (void) loadImage:(UIImage *)image hasAlphaMasking:(BOOL)hasAlphaMasking
{
    if (IS_IPAD && _popover) {
        [_popover dismissPopoverAnimated:YES];
    }
    
    [self.imagePainter loadImage:image hasAlphaMasking:hasAlphaMasking];
    self.imagePainter.zoomScale = 1.0;
    
    Color *color = [PainterUtility currentColor];
    
    if (color) {
        [color setLayerColorInfo:self.imagePainter.editLayer];
        self.imagePainter.paintColor = color.uiColor;
    }
    
    [self setDefaultToolMode];
}

#pragma mark -
#pragma mark Button Clicks, Other Actions

- (IBAction)cameraPressed:(id)sender {
    [self takePhoto:YES];
    [self showPaintTools:NO];
}

- (IBAction)libraryPressed:(id)sender {
    [self getPhoto:YES];
    [self showPaintTools:NO];
}

- (IBAction)undoClicked:(id)sender;
{
    [self.imagePainter stepBackward];
    [self showPaintTools:NO];
}

- (IBAction)toolClicked:(id)sender {
    [self showPaintTools:self.paintTools.hidden];
}

- (IBAction)saveClicked:(id)sender {
#ifdef SAVE_TEST
    [self saveTest:SAVE_TEST];
    return;
#endif
    
    __unsafe_unretained PaintViewController *weakSelf = self;
    [PainterUtility popupWithStatus:@"Saving Photo"
                              queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                              block:^{
                                  UIImage *modifiedImage = [weakSelf.imagePainter getRenderedImage];
                                  modifiedImage = [weakSelf addColorToImage:modifiedImage];
                                  [PainterUtility saveImageToPhotoAlbum:modifiedImage];
                              }];

}

- (UIImage*) addColorToImage:(UIImage*)image;
{
    UIGraphicsBeginImageContext(image.size);
    [image drawAtPoint:CGPointZero];
    
    UIColor *currentColor = [PainterUtility currentUIColor];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //draw background rect
	CGContextSetRGBFillColor(context, 0, 0, 0, 0.8);
	CGContextFillRect(context, CGRectMake(0.0, image.size.height - 25, image.size.width, 25));
    
    CGContextSetFillColorWithColor(context, currentColor.CGColor);
	CGContextFillRect(context, CGRectMake(5.0, image.size.height - 22, 20, 20));
    
    //draw text
    NSString *text = NULL;
    
    if ([PainterUtility currentColor]) {
        Color *color = [PainterUtility currentColor];
        text = [NSString stringWithFormat:@"%@ - %@ %@",
         color.category.brand.name,
         color.name,
         color.code];
    } else {
        text = @" ";
    }
    
    CGPoint textPoint = CGPointMake(30, image.size.height - 20);
    UIFont *font = [UIFont boldSystemFontOfSize:12];
    CGRect rect = CGRectMake(textPoint.x, textPoint.y, image.size.width, image.size.height);
    [[UIColor whiteColor] set];
    [text drawInRect:CGRectIntegral(rect) withFont:font];
    
    //draw branding
    UIFont *brandFont = [UIFont italicSystemFontOfSize:12];
    [[UIColor whiteColor] set];
    
    CGPoint brandPoint = CGPointMake(image.size.width - 145, image.size.height - 20);
    CGRect brandRect = CGRectMake(brandPoint.x, brandPoint.y, image.size.width, image.size.height);
    if (IS_IPAD) {
        [@"Paint Harmony for iPad" drawInRect:CGRectIntegral(brandRect) withFont:brandFont];
    } else {
        [@"Paint Harmony for iPhone" drawInRect:CGRectIntegral(brandRect) withFont:brandFont];
    }
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (IBAction)bucketFillSelected:(id)sender {
    self.imagePainter.toolMode = ToolModeFill;
    [self updateTools];
    [self showPaintTools:NO];
}

- (IBAction)rectangleSelected:(id)sender {
    self.imagePainter.toolMode = ToolModeRectangle;
    [self.imagePainter showRectangle];
    [self updateTools];
    [self showPaintTools:NO];
}

- (IBAction)paintbrushSelected:(id)sender {
    self.imagePainter.toolMode = ToolModePaintbrush;
    [self updateTools];
    [self showPaintTools:NO];
}

- (IBAction)eraserSelected:(id)sender {
    self.imagePainter.toolMode = ToolModeEraser;
    [self updateTools];
    [self showPaintTools:NO];
}

- (void)drawerButtonClicked;
{
    [self.drawer toggleDrawer:!self.drawer.isOpen];
}

#pragma mark -
#pragma mark Drawer
- (BOOL)drawerIsOpen
{
    return !self.sideDrawer.view.hidden;
}

- (void) CBDrawerController_drawerWillOpen:(id)drawer;
{
    [self.sideDrawer sidebarWillAppear];
}

- (void) CBDrawerController_drawerWillClose:(id)drawer;
{
    [self.sideDrawer sidebarWillDisappear];
}

- (void) CBDrawerController_drawerDidOpen:(id)drawer;
{
    
}

- (void) CBDrawerController_drawerDidClose:(id)drawer;
{
    [self updateInstructions];
}

#pragma mark -
#pragma mark SideBar Delegate Methods

- (void) SideBar_ShowLibrary;
{
    [self.drawer toggleDrawer:NO];
    
    [self getPhoto:YES];
}

- (void) SideBar_PickedColorCategory:(ColorCategory *)colorCategory;
{
    __weak PaintViewController * weakSelf = self;
    [self.drawer toggleDrawer:NO block:^{
        self.colorTabs.colorCategory = colorCategory;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [weakSelf.colorTabs setCurrentTab:ColorTabSwatches];
            [weakSelf setDefaultToolMode];
        });
    } animate:YES];
}

- (void) SideBar_OpenedColorPicker;
{
    [self.drawer toggleDrawer:NO];
    [self setDefaultToolMode];
}

- (void) SideBar_OpenedHarmony:(BOOL)isCamera;
{
    [self.drawer toggleDrawer:NO];
    [self paintbrushSelected:nil];
    
    double delayInSeconds = IS_IPHONE_5 ? 0.1 : 0.5;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        HarmonyViewController *harmony = [[HarmonyViewController alloc] initWithNibName:nil bundle:nil];
        harmony.delegate = self;
        harmony.parent = self;
        harmony.color = [PainterUtility currentColor];
        
        harmony.modalPresentationStyle = UIModalPresentationPageSheet;
        harmony.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        
        [self presentViewController:harmony animated:NO completion:^{
            if (isCamera) {
                [harmony showCamera:YES];
            } else {
                [harmony showLibrary:NO];
            }
        }];
    });
    
}

- (void) SideBar_ShowFavorites;
{
    
}

- (void) SideBar_PickedColor:(Color *)color;
{
    [self SelectColor_PickedColor:color uiColor:color.uiColor];
//    self.pickerMode = PickerModeSwatches;
//    [self openSwatches:YES];
    
    [self.drawer toggleDrawer:NO];
}

- (void) SideBar_EmailMore;
{
    __unsafe_unretained PaintViewController *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        if (IS_IPAD) {
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        }
        mailer.mailComposeDelegate = weakSelf;
        [mailer setSubject:@"Product Recomendation"];
        
        [mailer setToRecipients:[NSArray arrayWithObject:CONTACT_EMAIL]];
        
        [weakSelf presentModalViewController:mailer animated:YES];
    });
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
}

- (void) SideBar_LoadImage:(UIImage *)image;
{
    [SVProgressHUD showWithStatus:@"Loading"];
    [self.drawer toggleDrawer:NO block:^{
        [self loadImage:image hasAlphaMasking:YES];
        [SVProgressHUD dismiss];
    } animate:YES];
}

- (void)ColorViewController_colorClicked:(Color *)color;
{
    [self SelectColor_PickedColor:color uiColor:color.uiColor];
    [self displayColorDetails:color cameFromSelf:NO];
}

- (IBAction)colorNameClicked:(id)sender {
    [self displayColorDetails:[PainterUtility currentColor] cameFromSelf:YES];
}

- (void)addLayerClicked:(id)sender; {
    if (![self.imagePainter canAppendNewLayer]) return;
    
    [self.imagePainter appendNewLayer];
    BOOL hasSeenLayerDirections = [[SettingsModel getInstance] hasSeenLayerDirections];
    
    if (!hasSeenLayerDirections) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"New Layer Created"
                                                          message:@"A new color layer has been created.\n Changes in color will not affect the existing paint below it.\n\n Feel free to select a new color and paint either new walls or existing areas."
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        [[SettingsModel getInstance] setHasSeenLayerDirections:YES];
    } else {
        [SVProgressHUD showSuccessWithStatus:@"New Layer Created"];
    }
}

- (void)displayColorDetails:(Color *)color cameFromSelf:(BOOL)cameFromSelf;
{
    ColorViewController *colorVC = [[ColorViewController alloc] initWithNibName:nil bundle:nil];
    colorVC.parent = self;
    colorVC.color = color;
    colorVC.delegate = self;
    
    colorVC.modalPresentationStyle = UIModalPresentationPageSheet;
    colorVC.modalTransitionStyle = cameFromSelf ? UIModalTransitionStyleFlipHorizontal : UIModalTransitionStyleCoverVertical;
    
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
    
    [self presentViewController:colorVC animated:cameFromSelf completion:NULL];
}

- (void)closeAllOverlays:(id)except {
    if (except != self.layersMenu) [self showLayersMenu:NO];
    if (except != self.editLayersMenu) [self showEditLayersMenu:NO];
    if (except != self.paintTools) [self showPaintTools:NO];
    
    if (except != self.colorTabs) [self.colorTabs setCurrentTab:ColorTabNone];
}

#pragma mark LAYER EDITING

- (void)createLayersMenus {
    int extraWidth = 15;
    int yOffset = 20;
    
    CGRect frame = CGRectMake(self.showLayersButton.frame.origin.x - extraWidth,
                              self.toolbar.frame.origin.y - self.showLayersButton.frame.size.height - yOffset,
                              100,
                              100);
    
    self.layersMenu = [[UIScrollView alloc] initWithFrame:frame];
    
    self.layersMenu.backgroundColor = [UIColor clearColor];
    self.layersMenuContent = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self.layersMenu addSubview:self.layersMenuContent];
    [self.view insertSubview:self.layersMenu belowSubview:self.paintTools];
    
    int editLayersMenuW = 300;
    self.editLayersMenu = [[UIView alloc] initWithFrame:CGRectMake(100,
                                                                   self.toolbar.frame.origin.y - self.showLayersButton.frame.size.height - yOffset,
                                                                   editLayersMenuW,
                                                                   self.showLayersButton.frame.size.height)];
    self.editLayersMenuScroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,
                                                                                 editLayersMenuW,
                                                                                 self.showLayersButton.frame.size.height)];
    self.editLayersMenuScroller.autoresizesSubviews = false;
    self.editLayersMenu.backgroundColor = [UIColor whiteColor];
    self.editLayersMenuContent = [[UIView alloc] initWithFrame:CGRectMake(0, 0, editLayersMenuW, 100)];
    [self.editLayersMenuScroller addSubview:self.editLayersMenuContent];
    
    [self.editLayersMenu addSubview:self.editLayersMenuScroller];
    
    self.editLayersMenuCancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.editLayersMenuCancelButton.frame = CGRectMake(0, 0, 100, 40);
    self.editLayersMenuCancelButton.frame = CGRectCenterX(self.editLayersMenuCancelButton.frame, self.editLayersMenu.frame);
    [self.editLayersMenuCancelButton addTarget:self action:@selector(cancelEditLayersClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.editLayersMenuCancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [self.editLayersMenu addSubview:self.editLayersMenuCancelButton];
    
    [self.view addSubview:self.editLayersMenu];
    
    self.editLayersMenu.hidden = YES;
}

- (void)showLayersMenu:(BOOL)show {
    if (show) {
        [self closeAllOverlays:self.layersMenu];
        [self buildLayersMenu];
    }
    
    double closePosition = self.toolbar.frame.origin.y;
    double openPosition = self.toolbar.frame.origin.y - self.layersMenu.frame.size.height - 5;
    
    BOOL isOpen = !self.layersMenu.hidden;
    
    if (isOpen == show) return;
    
    CGFloat xPos = MIN(self.view.frame.size.width - self.layersMenu.frame.size.width - 5, self.showLayersButton.frame.origin.x);
    self.layersMenu.frame = CGRectSetX(xPos, self.layersMenu.frame);
   
    self.showLayersButton.selected = show;
    self.layersMenu.frame = CGRectSetY(show ? closePosition : openPosition, self.layersMenu.frame);
    self.layersMenu.hidden = NO;
    
    [UIView animateWithDuration:0.1 animations:^{
        self.layersMenu.frame = CGRectSetY(show ? openPosition : closePosition, self.layersMenu.frame);
    } completion:^(BOOL finished) {
        self.layersMenu.hidden = !show;
    }];
}

- (void)buildLayersMenu {
    
    //destroy existing
    for (UIView *subview in self.layersMenuContent.subviews) {
        [subview removeFromSuperview];
    }
    
    int yOffset = 0;
    
    NSArray * layers = self.imagePainter.stillImage.layers;
    
    for (int layerNumber = 1; layerNumber<[layers count]; layerNumber++) {
        CBLayer *layer = [layers objectAtIndex:layerNumber];
        
        BOOL isCurrentLayer = layerNumber == self.imagePainter.editLayerIndex;
        
        UIView *menuItem = [self createToolItem:(layerNumber == 1 ? 0 : 2) tag:layerNumber
                                          title:[self getLayerDescription:layerNumber]
                                       selector:@selector(layerButtonClicked:) color:layer.fillColor];
        menuItem.frame = CGRectSetY(yOffset, menuItem.frame);
        if (isCurrentLayer) {
            UIButton *layerButton = (UIButton *) [menuItem viewWithTag:layerNumber];
            layerButton.enabled = NO;
        }
        
        [self.layersMenuContent addSubview:menuItem];
        yOffset += menuItem.frame.size.height;
    }
    
    if ([layers count] > 2) {
        
        UIView *editColorsItem = [self createToolItem:2 tag:-1
                                          title:@"Edit Colors"
                                       selector:@selector(editLayersButtonClicked:) color:nil];
        
        editColorsItem.frame = CGRectSetY(yOffset, editColorsItem.frame);
        [self.layersMenuContent addSubview:editColorsItem];
        
        yOffset += editColorsItem.frame.size.height;
    }
    
    UIView *newLayerItem = [self createToolItem:1 tag:-1
                                            title:@"New Layer"
                                         selector:@selector(addLayerButtonClicked:) color:nil];
    newLayerItem.frame = CGRectSetY(yOffset, newLayerItem.frame);
    [self.layersMenuContent addSubview:newLayerItem];
    
    yOffset += newLayerItem.frame.size.height;
    
    self.layersMenuContent.frame = CGRectSetH(yOffset, self.layersMenuContent.frame);
    self.layersMenuContent.frame = CGRectSetW(155, self.layersMenuContent.frame);
    
    self.layersMenu.contentSize = self.layersMenuContent.frame.size;
    self.layersMenu.frame = CGRectSetH(MIN(self.layersMenuContent.frame.size.height, self.imagePainter.frame.size.height), self.layersMenu.frame);
    self.layersMenu.frame = CGRectSetW(self.layersMenuContent.frame.size.width, self.layersMenu.frame);
}

- (UIView *)createToolItem:(int)level tag:(int)tag title:(NSString *)title selector:(SEL)selector color:(UIColor *)color {
    UIImage *bgImage, *bgImageOn;
    
    switch (level) {
        case 0:
            bgImage = [UIImage imageNamed:@"tool_menu_top.png"];
            bgImageOn = [UIImage imageNamed:@"tool_menu_top_on.png"];
            break;
        case 1:
            bgImage = [UIImage imageNamed:@"tool_menu_bottom.png"];
            bgImageOn = [UIImage imageNamed:@"tool_menu_bottom_on.png"];
            break;
        default:
            bgImage = [UIImage imageNamed:@"tool_menu_middle.png"];
            bgImageOn = [UIImage imageNamed:@"tool_menu_middle_on.png"];
            break;
    }
    
    UIView *toolItem = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,
                                                                  bgImage.size.width,
                                                                  bgImage.size.height)];
    toolItem.backgroundColor = [UIColor clearColor];
    
    
    UIButton *menuItemButton = [[UIButton alloc] initWithFrame:toolItem.frame];
    
    [menuItemButton setBackgroundImage:bgImage forState:UIControlStateNormal];

    
    menuItemButton.tag = tag;
    [menuItemButton addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    [menuItemButton setTitle:title forState:UIControlStateNormal];
    [menuItemButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [menuItemButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [menuItemButton setBackgroundImage:bgImageOn forState:UIControlStateHighlighted];
    [menuItemButton setBackgroundImage:bgImageOn forState:UIControlStateDisabled];
    menuItemButton.titleLabel.font = [UIFont systemFontOfSize:12];
    menuItemButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    menuItemButton.contentEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
    menuItemButton.adjustsImageWhenHighlighted = YES;
    
    [toolItem addSubview:menuItemButton];
    
    if (color) {
        UIView *colorView = [[UIView alloc] initWithFrame:CGRectMake(5, 5, 20, 20)];
        colorView.backgroundColor = color;
        [toolItem addSubview:colorView];
        colorView.frame = CGRectCenterY(colorView.frame, toolItem.frame);
    }
    
    return toolItem;
}

- (NSString *)getLayerDescription:(int)layerIndex {
    CBLayer *layer = [self.imagePainter.stillImage.layers objectAtIndex:layerIndex];
    
    Color *color = [Color colorForLayer:layer];
    
    if (color) {
        return color.name;
    }
    
    return @"Color";
}

- (void)showEditLayersMenu:(BOOL)show {
    if (show) {
        [self closeAllOverlays:self.editLayersMenu];
        [self buildEditLayersMenu];
    }
    self.editLayersMenu.hidden = !show;
}

- (void)buildEditLayersMenu {
    //destroy existing
    for (UIView *subview in self.editLayersMenuContent.subviews) {
        [subview removeFromSuperview];
    }
    
    int yOffset = 0;
    
    NSArray * layers = self.imagePainter.stillImage.layers;
    for (int layerNumber = 1; layerNumber<[layers count]; layerNumber++) {
        CBLayer *layer = [layers objectAtIndex:layerNumber];
        
        //BOOL isCurrentLayer = layerNumber == self.imagePainter.editLayerIndex;
        
        UIView *menuItem = [[UIView alloc] initWithFrame:CGRectMake(0, yOffset,
                                                                    300,
                                                                    50)];
        
        UIView *colorView = [[UIView alloc] initWithFrame:CGRectMake(5, 0, 25, 25)];
        colorView.frame = CGRectCenterY(colorView.frame, menuItem.frame);
        
        colorView.frame = CGRectCenterY(colorView.frame, menuItem.frame);
        colorView.backgroundColor = layer.fillColor;
        [menuItem addSubview:colorView];
        
        UILabel *menuItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(colorView.frame) + 10, 0, 200, 35)];
        menuItemLabel.frame = CGRectCenterY(menuItemLabel.frame, menuItem.frame);
        menuItemLabel.text = [self getLayerDescription:layerNumber];
        [menuItem addSubview:menuItemLabel];
        
        UIButton *menuItemButton = [[UIButton alloc] initWithFrame:CGRectMake(menuItem.frame.size.width - 65, 0,
                                                                              60, 35)];
        menuItemButton.frame = CGRectCenterY(menuItemButton.frame, menuItem.frame);
        menuItemButton.tag = layerNumber;
        [menuItemButton addTarget:self action:@selector(removeLayerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [menuItemButton setTitle:@"Delete" forState:UIControlStateNormal];
        [menuItemButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        menuItemButton.titleLabel.font = [UIFont systemFontOfSize:12];
        menuItemButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        menuItemButton.showsTouchWhenHighlighted = YES;
        menuItemButton.backgroundColor = DELETE_BUTTON_COLOR;
        [menuItem addSubview:menuItemButton];
        
        [self.editLayersMenuContent addSubview:menuItem];
        yOffset += menuItem.frame.size.height;
    }
    
    self.editLayersMenuContent.frame = CGRectSetH(yOffset, self.editLayersMenuContent.frame);
    self.editLayersMenuScroller.contentSize = self.editLayersMenuContent.frame.size;
    
    int cancelButtonPadding = 10;
    int maxHeight = self.imagePainter.frame.size.height - self.editLayersMenuCancelButton.frame.size.height - 2 * cancelButtonPadding;
    self.editLayersMenuScroller.frame = CGRectSetH(MIN(self.editLayersMenuScroller.contentSize.height, maxHeight), self.editLayersMenuScroller.frame);
    
    self.editLayersMenuCancelButton.frame = CGRectSetY(CGRectGetMaxY(self.editLayersMenuScroller.frame) + cancelButtonPadding,
                                                       self.editLayersMenuCancelButton.frame);
    
    self.editLayersMenu.frame = CGRectSetH(CGRectGetMaxY(self.editLayersMenuCancelButton.frame) + 10, self.editLayersMenu.frame);
    self.editLayersMenu.frame = CGRectCenter(self.editLayersMenu.frame, self.view.frame);
}

- (IBAction)showLayersButtonClicked:(id)sender {
    [self showLayersMenu:self.layersMenu.hidden];
}

- (void)addLayerButtonClicked:(id)sender {
    [self.imagePainter appendNewLayer];
    [self editLayerChanged];
    [self showLayersMenu:NO];
    [self.colorTabs setCurrentTab:ColorTabSwatches];
    [self setDefaultToolMode];
}

- (void)editLayerChanged {
    //[self.showLayersButton setTitle:[NSString stringWithFormat:@"Colour %d", self.imagePainter.editLayerIndex] forState:UIControlStateNormal];
    
    Color *color = [Color colorForLayer:self.imagePainter.editLayer];
    [PainterUtility setCurrentColor:color uiColor:self.imagePainter.paintColor sender:self.imagePainter];
}

- (void)layerButtonClicked:(id)sender {
    UIButton * layerButton = sender;
    self.imagePainter.editLayerIndex = MAX(layerButton.tag, 1);
    [self editLayerChanged];
    [self showLayersMenu:NO];
}

- (void)editLayersButtonClicked:(id)sender {
    [self showEditLayersMenu:YES];
}

- (void)cancelEditLayersClicked:(id)sender {
    [self showEditLayersMenu:NO];
}

- (void)removeLayerButtonClicked:(id)sender {
    UIButton * layerButton = sender;
    [self.imagePainter removeLayerAtIndex:layerButton.tag];
    [self editLayerChanged];
    [self showEditLayersMenu:NO];
}


@end
