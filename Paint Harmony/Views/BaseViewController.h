//
//  BaseViewController.h
//  Home Decorator
//
//  Created by Joel Teply on 1/3/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface BaseViewController : UIViewController

- (void) colorChangedNotification:(id)sender;

@property (assign, nonatomic) BOOL hideStatusBar;

@end
