//
//  ColorViewController.m
//  AugmentedRoom
//
//  Created by Joel Teply on 11/3/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "ColorViewController.h"
#import "Brand.h"
#import "Color.h"
#import "ColorCategory.h"
#import "Color+Extended.h"
#import "PainterUtility.h"
#import "ColorScroller.h"
#import "SVProgressHUD.h"
#import "SettingsModel.h"

#define USE_TEXTURE 1

@interface ColorViewController () {
    UIImage *_texture;
    
    NSMutableArray *_colorScrollers;
    ColorScroller *_analoguesView;
    ColorScroller *_complementsView;
    
    BOOL _busyCollapsing, _autolayoutCompleted;
    UIImage *_bigTexture;
}

@property (nonatomic, retain) UIImageView *bigImageView;
@property (strong, nonatomic) IBOutlet UIView *favoritesInstructionsArrow;

@end

@implementation ColorViewController

@synthesize bigImageView = _bigImageView;
@synthesize favoritesInstructionsArrow = _favoritesInstructionsArrow;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _colorScrollers = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.favoriteButton.alpha = self.color.favorite ? 1.0 : 0.3;
    
    self.favoritesInstructions.hidden = [[SettingsModel getInstance] hasSeenFavoritesDirections];
    
    self.favoritesInstructionsArrow = [PainterUtility setupTutorialStep:self.favoritesInstructions above:YES centered:NO];
    
    //mark as witnessed
    [[SettingsModel getInstance] setHasSeenFavoritesDirections:YES];
    
    CGSize swatchSize = CGSizeMake(80, 80);
    if (IS_IPAD) {
        swatchSize = CGSizeMake(150, 150);
    }
    else if (IS_WIDESCREEN) {
        swatchSize = CGSizeMake(100, 100);
    }
    
    if (!PainterUtility.isLite) {
        //bottommost
        NSArray *complements = [CBColoring complementsForColor:self.color.uiColor count:5 angle:40.0f];
        
        _complementsView = [[ColorScroller alloc] initWithDelegate:self
                                                             title:@"Complementary Colors"
                                                            colors:[self getShades:complements count:4]
                                                           texture:nil
                                                              size:swatchSize];
        [self.view addSubview:_complementsView.view];
        [_colorScrollers addObject:_complementsView];
        
        //analogues
        NSArray *analogues = [CBColoring adjacentColors:self.color.uiColor count:5 angle:40];
        
        
        _analoguesView = [[ColorScroller alloc] initWithDelegate:self
                                                           title:@"Analogous Colors"
                                                          colors:[self getShades:analogues count:4]
                                                         texture:nil
                                                            size:swatchSize];
        
        [self.view addSubview:_analoguesView.view];
        [_colorScrollers addObject:_analoguesView];
    }
    

    //[PainterUtility gravityForViews:_colorScrollers isReversed:NO];
    //complementary
//    NSArray *complements = [CBColoring complementsForColor:self.color.uiColor includeSplitComplements:YES];
//    
//    ColorScroller *complementsView = [[ColorScroller alloc] initWithColorViewSize:CGSizeMake(100, 100)];
//    [complementsView loadColors:complements withTexture:texture];
//    
//    [self.view addSubview:complementsView.view];
    
    
    //NSArray *complements = [CBColoring]
}

- (NSArray *)getShades:(NSArray *)colors count:(int)count
{
    NSMutableArray *shades = [NSMutableArray array];
    
    for (UIColor *color in colors) {
        [shades addObjectsFromArray:[CBColoring shadesOfColor:color count:count]];
    }
    
    return shades;
}

- (void) autolayoutCompleted;
{
    if (_autolayoutCompleted) return;
    _bigTexture = [UIImage imageNamed:IS_IPAD ? @"standard_texture_iPad.jpg" : @"standard_texture.jpg"];
    
    self.bigImageView = [PainterUtility makeColorTexture:_bigTexture withColor:self.color.uiColor withFrame:self.view.frame];

    //    NSLog(@"Frame = %f,%f", bigColor.frame.size.width, bigColor.frame.size.height);
    [self.view insertSubview:self.bigImageView atIndex:0];
    
    if (!PainterUtility.isLite) {
//    [_complementsView prepareForCollapse];
//    [_complementsView performCollapse];
//    
//    [_analoguesView prepareForCollapse];
//    [_analoguesView performCollapse];
    
    _complementsView.view.frame = CGRectSetY(self.view.frame.size.height
                                             - _complementsView.view.frame.size.height,
                                             _complementsView.view.frame);
    
    _analoguesView.view.frame = CGRectSetY(CGRectGetMinY(_complementsView.view.frame)
                                           - _analoguesView.view.frame.size.height,
                                           _analoguesView.view.frame);
    }
    
    _autolayoutCompleted = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self autolayoutCompleted];
    
    self.view.backgroundColor = self.color.uiColor;
    self.brandLabel.text = [NSString stringWithFormat:@"%@ - %@",
                            self.color.category.brand.name,
                            self.color.category.name];
    self.colorNameLabel.text = self.color.name;
    
    self.colorNumberLabel.text = [NSString stringWithFormat:@"#%@", self.color.code];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)ColorScroller_titleClicked:(ColorScroller *)colorScroller;
{
    if (_busyCollapsing) return;
    _busyCollapsing = YES;
    [colorScroller prepareForCollapse];
    
    [UIView animateWithDuration:0.25 animations:^{
        [colorScroller performCollapse];
        if (colorScroller == _complementsView) {
            _analoguesView.view.frame = CGRectSetY(CGRectGetMinY(_complementsView.view.frame)
                                                   - _analoguesView.view.frame.size.height,
                                                   _analoguesView.view.frame);
        }
    } completion:^(BOOL finished) {
        _busyCollapsing = NO;
    }];
}

- (void)ColorScroller_colorClicked:(ColorScroller *)colorScroller color:(UIColor*)uiColor;
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ColorViewController_colorClicked:)]) {
        [SVProgressHUD showWithStatus:@"Locating Closest Match"];
        ColorViewController *weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            Color *color = [PainterUtility closestMatchForUIColor:uiColor
                                                            brand:self.color.category.brand
                                                         category:nil];
            NSLog(@"Closest color is %@", color.name);
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [weakSelf.delegate ColorViewController_colorClicked:color];
            });
        });
        
    }
}

//- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event
//{
//    UITouch *touch = [touches anyObject];
//    
//    if ([self isTouch:touch withinView:self.complementaryColorView]) {
//        if ([self isTouch:touch withinView:self.complementaryColorTitleView]){
//            [self toggleColorView:self.complementaryColorView titleView:self.complementaryColorTitleView];
//        }
//        
//    } else if ([self isTouch:touch withinView:self.accentColorView]){
//        if ([self isTouch:touch withinView:self.accentColorTitleView]){
//            [self toggleColorView:self.accentColorView titleView:self.accentColorTitleView];
//        }
//    } else {
//        [self dismissModalViewControllerAnimated:YES];
//    }
//}

//- (void)toggleColorView:(UIView *)container titleView:(UIView *)titleView
//{
//    BOOL isOpen = container.frame.size.height > titleView.frame.size.height;
//    
//    float openHeight = 0;
//    
//    for (UIView *subview in container.subviews) {
//        if (subview.frame.origin.y >= titleView.frame.size.height) {
//            openHeight = CGRectGetMaxY(subview.frame);
//            break;
//        }
//    }
//    
//    float heightChange;
//    if (isOpen) {
//        heightChange = titleView.frame.size.height - openHeight;
//    } else {
//        heightChange = openHeight - titleView.frame.size.height;
//    }
//    
//    float yEnd = container.frame.origin.y - heightChange;
//    
//    CGRect endFrame;
//    if (isOpen) {
//        endFrame = CGRectSetH(titleView.frame.size.height, container.frame);
//    } else {
//        endFrame = CGRectSetH(openHeight, container.frame);
//    }
//    endFrame = CGRectSetY(yEnd, endFrame);
//    
//    [UIView animateWithDuration:0.25 animations:^{
//        container.frame = endFrame;
//        if (container == self.complementaryColorView) {
//            self.accentColorView.frame = CGRectSetY(self.accentColorView.frame.origin.y - heightChange,
//                                                    self.accentColorView.frame);
//        }
//    } completion:^(BOOL finished) {
//        
//    }];
//}


- (void)viewDidUnload {
    [self setBrandLabel:nil];
    [self setColorNameLabel:nil];
    [self setColorNumberLabel:nil];
    [self setFavoriteButton:nil];
    [self setActionButton:nil];
    [self setFavoritesInstructions:nil];
    [super viewDidUnload];
}

- (IBAction)doneClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (BOOL)isFavoriteColor
{
    return [self.color.favorite boolValue];
}

- (IBAction)favoriteClicked:(id)sender {
    //may want to move this later
    [PainterUtility setFavoriteColor:self.color isFavorite:![self isFavoriteColor]];
    
    self.favoritesInstructions.hidden = true;
    self.favoritesInstructionsArrow.hidden = true;
    
    self.favoriteButton.alpha = [self isFavoriteColor] ? 1.0 : 0.3;
}

- (IBAction)actionClicked:(id)sender {
    
    self.isPresentingModal = YES;
    [self sendEmailAsync];
}

- (void)sendEmailAsync
{
    __unsafe_unretained ColorViewController *weakSelf = self;
    [PainterUtility popupWithStatus:@"Generating Email"
                              queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                                                   block:^{
        [weakSelf sendEmail];
    }];
}

- (void)sendEmail
{
    
    __unsafe_unretained ColorViewController *weakSelf = self;
    
    UIImage *paintSwatch;
    if (self.bigImageView.image) {
        paintSwatch = self.bigImageView.image;
    }
    else {
        paintSwatch = [PainterUtility makeColorTextureImage:_bigTexture withColor:self.color.uiColor withFrame:self.view.frame];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        if (IS_IPAD) {
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        }
        mailer.mailComposeDelegate = weakSelf;
        [mailer setSubject:[NSString stringWithFormat:@"%@ by %@", weakSelf.color.name, weakSelf.color.category.brand.name]];
        
        NSData *imageData = UIImagePNGRepresentation(paintSwatch);
        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"color.png"];
        
        //body
        NSString *emailBody = [NSString stringWithFormat:@"<b>%@ - %@</b><br />\n\
                               <b>Color:</b> %@ #%@ <br /><br />%@",
                               weakSelf.color.category.brand.name,
                               weakSelf.color.category.name,
                               weakSelf.color.name, weakSelf.color.code,
                               [PainterUtility appStoreHTML]];
        
        [mailer setMessageBody:emailBody isHTML:YES];
        
        [weakSelf presentModalViewController:mailer animated:YES];
    });
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
    
    self.isPresentingModal = NO;
    
}

@end
