//
//  FirstViewController.h
//  Wall Painter
//
//  Created by Joel Teply on 11/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ColorTabsViewController.h"
#import "SideBarViewController.h"
#import "ColorViewController.h"
#import <MessageUI/MessageUI.h>
#import <iAd/iAd.h>
#import "ImagePainter.h"

@class ColorTabsViewController, UAImagePickerController;

@interface PaintViewController : BaseViewController
    <UIImagePickerControllerDelegate, UINavigationControllerDelegate,
    MFMailComposeViewControllerDelegate, UIPopoverControllerDelegate,
    ColorTabDelegate, SideBarDelegate, CBDrawerControllerDelegate,
    ColorViewControllerDelegate> {
}

@property (weak, nonatomic) IBOutlet UIView *primaryView;
@property (weak, nonatomic) IBOutlet ImagePainter *imagePainter;

@property (weak, nonatomic) IBOutlet UIView *paintTools;
@property (weak, nonatomic) IBOutlet UIButton *paintToolbarButton;

@property (strong, nonatomic) IBOutlet UIView *toolbar;

@property (weak, nonatomic) IBOutlet UIView *colorNameView;
@property (strong, nonatomic) IBOutlet UIButton *colorNameButton;

@property (strong, nonatomic) IBOutlet UIView *currentColorView;

@property (weak, nonatomic) IBOutlet UIButton *undoButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@property (weak, nonatomic) IBOutlet UIButton *showLayersButton;

@property (weak, nonatomic) IBOutlet UIView *instructionsView;
@property (weak, nonatomic) IBOutlet UILabel *instructionsLabel;

@property (strong, nonatomic) UAImagePickerController *camera;
@property (strong, nonatomic) ColorTabsViewController *colorTabs;
@property (assign, nonatomic) BOOL unsavedWork;
@property (strong, nonatomic) CBDrawerController *drawer;
@property (strong, nonatomic) SideBarViewController *sideDrawer;
@property dispatch_source_t commitTimer;
@property BOOL commitTimingOut;

@property (strong, nonatomic) UIScrollView *layersMenu;
@property (strong, nonatomic) UIView *layersMenuContent;

@property (strong, nonatomic) UIView *editLayersMenu;
@property (strong, nonatomic) UIScrollView *editLayersMenuScroller;
@property (strong, nonatomic) UIView *editLayersMenuContent;
@property (strong, nonatomic) UIButton *editLayersMenuCancelButton;

@property (weak, nonatomic) IBOutlet UIView *rectangleToolOptions;
@property (weak, nonatomic) IBOutlet UILabel *rectangleToolInstructions;
@property (weak, nonatomic) IBOutlet UIView *overlayTutorialView;

- (IBAction)cameraPressed:(id)sender;
- (IBAction)undoClicked:(id)sender;
- (IBAction)toolClicked:(id)sender;
- (IBAction)saveClicked:(id)sender;
- (IBAction)colorNameClicked:(id)sender;
- (IBAction)commitRectangleClicked:(id)sender;
- (IBAction)decommitRectangleClicked:(id)sender;
- (IBAction)tutorialDismissed:(id)sender;

- (IBAction)bucketFillSelected:(id)sender;
- (IBAction)rectangleSelected:(id)sender;
- (IBAction)paintbrushSelected:(id)sender;
- (IBAction)eraserSelected:(id)sender;

- (IBAction) dismissInstructions:(id)sender;

@end
