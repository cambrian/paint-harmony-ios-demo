//
//  HarmonyViewController.m
//  VirtualPainter
//
//  Created by Joel Teply on 11/12/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "HarmonyViewController.h"
#import "ColorScroller.h"
#import "SVProgressHUD.h"
#import "PainterUtility.h"
#import "Brand.h"
#import "Color.h"
#import "Color+Extended.h"
#import "ColorCategory.h"
#import "SettingsModel.h"
#import "UAImagePickerController.h"

static BOOL isUnloading = NO;

@interface ImageDropperColor : UIView

@property (retain, nonatomic) UIColor *color;

@end

@implementation ImageDropperColor

@synthesize color = _color;

- (void)setColor:(UIColor *)color
{
    _color = color;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect;
{
        //more stuff
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, self.color.CGColor);
    CGContextFillEllipseInRect(context, rect);
}

@end

@interface HarmonyViewController () {
    dispatch_source_t _timer;
    NSArray *_brands;
    dispatch_queue_t _finder_queue;
    BOOL _autolayoutCompleted;
    BOOL _shouldDisappear;
}

@property (retain, nonatomic) Brand *selectedBrand;
@property (retain, nonatomic) ColorCategory *selectedCategory;
@property (assign, nonatomic) __block int finderCount;
@property (assign, nonatomic) BOOL isCameraMode;
@property (retain, nonatomic) UAImagePickerController *camera;
@property (retain, nonatomic) __block CBImage *cbImage;
@property (retain, nonatomic) ColorScroller *colorScroller;
@property (retain, nonatomic) UIPopoverController *popover;
@property (retain, nonatomic) UIView *imageDropper;
@property (retain, nonatomic) UIImageView *imageDropperIcon;
@property (retain, nonatomic) ImageDropperColor *imageDropperColor;

@end

@implementation HarmonyViewController

@synthesize finderCount = _finderCount;
@synthesize isCameraMode = _isCameraMode;
@synthesize camera = _camera;
@synthesize cbImage = _cbImage;
@synthesize colorScroller = _colorScroller;
@synthesize color = _color;
@synthesize popover = _popover;
@synthesize imageDropper = _imageDropper;
@synthesize imageDropperIcon = _imageDropperIcon;
@synthesize imageDropperColor = _imageDropperColor;
@synthesize selectedBrand = _selectedBrand;
@synthesize selectedCategory = _selectedCategory;

- (void)dealloc
{
    //isUnloading = YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    _finder_queue = dispatch_queue_create("com.cambriantech.virtual_painter.finder_queue", 0);
    isUnloading = NO;
    if (self) {
        // Custom initialization
        self.camera = [[UAImagePickerController alloc] init];
        self.camera.delegate = self;
//        self.camera.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        CGSize swatchSize = CGSizeMake(80, 80);
        
        if (IS_IPAD)
        {
            swatchSize = CGSizeMake(150, 150);
        } else if (IS_WIDESCREEN) {
            swatchSize = CGSizeMake(100, 100);
        }
        
        UIImage *texture = [UIImage imageNamed:@"standard_texture.jpg"];
        self.colorScroller = [[ColorScroller alloc] initWithDelegate:self
                                                             title:@"Colors Found in Image"
                                                            colors:nil
                                                           texture:texture
                                                              size:swatchSize];
        
        self.imageDropperIcon =  [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"color_dropper.png"]];
        self.imageDropper = [[UIView alloc] initWithFrame:self.imageDropperIcon.frame];
        self.imageDropperColor = [[ImageDropperColor alloc] initWithFrame:CGRectMake(10, 10, 80, 80)];
        self.imageDropperColor.opaque = NO;
        //self.imageDropperColor.color = [UIColor redColor];
        [self.imageDropper addSubview:self.imageDropperColor];
        [self.imageDropper addSubview:self.imageDropperIcon];
        
        self.imageDropper.hidden = YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:self.colorScroller.view];
    
    self.imageView.frame = CGRectSetH(self.view.frame.size.height, self.imageView.frame);
    
     [self.imageView addSubview:self.imageDropper];

    self.infoView.hidden = YES;
    
    self.categoryPickerView.hidden = YES;
    _brands = [PainterUtility allBrands];
    
    if (PainterUtility.isLite) {
        self.categoryView.hidden = YES;
    }
}

- (void) autolayoutCompleted
{
    if (_autolayoutCompleted) return;
    self.colorScroller.view.frame = CGRectSetW(self.view.frame.size.width, self.colorScroller.view.frame);
    self.colorScroller.view.frame = CGRectSetY(self.view.frame.size.height -
                                               self.toolbar.frame.size.height -
                                               self.colorScroller.view.frame.size.height,
                                               self.colorScroller.view.frame);
    
    self.imageView.frame = CGRectSetY(self.topControls.frame.size.height, self.imageView.frame);
    self.imageView.frame = CGRectSetH(self.view.frame.size.height
                                      - self.colorScroller.view.frame.size.height
                                      - self.topControls.frame.size.height,
                                      self.imageView.frame);
    
    if (!self.categoryView.hidden) {
        self.infoView.frame = CGRectSetY(CGRectGetMaxY(self.categoryView.frame), self.infoView.frame);
    }
    
    _autolayoutCompleted = YES;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self autolayoutCompleted];
    
    [self updateSelectedLabel];
    
    self.color = self.color;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_shouldDisappear) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (void) updateSelectedLabel {

    NSString * label = NULL;
    
    if (self.selectedCategory) {
        label = [NSString stringWithFormat:@"%@ - %@ >", self.selectedCategory.brand.name, self.selectedCategory.name];
    }
    else if (self.selectedBrand) {
        label = [NSString stringWithFormat:@"All %@ Colors >", self.selectedBrand.name];
    }
    else {
        label = @"Search Within: All Colors >";
    }
    
    [self.categoryButton setTitle:label forState:UIControlStateNormal];
}

- (IBAction)backClicked:(id)sender {
    if (self.color) {
        [self.delegate SelectColor_PickedColor:self.color uiColor:self.color.uiColor];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark -
#pragma mark Camera Logic

- (void)imagePickerController:(UIImagePickerController *)pker
    didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
    UIImage *largeImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [self.camera dismissModalViewControllerAnimated:NO];
    if (IS_IPAD) {
        [self.popover dismissPopoverAnimated:YES];
    }
    
    UIImage *smallerImage = [CBImage image:largeImage
                         constrainedToSize:CGSizeMake(1024, 1024)];
        
    self.infoView.hidden = [[SettingsModel getInstance] hasSeenHarmonyDirections];
    [[SettingsModel getInstance] setHasSeenHarmonyDirections:YES];
    
    self.cbImage = [[CBImage alloc] initWithUIImage:smallerImage];
    self.imageView.image = smallerImage;
    
    self.isPresentingModal = NO;
    
    __unsafe_unretained HarmonyViewController *weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [weakSelf harmonyEffect];
    });
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    if (IS_IPAD) {
        [self.popover dismissPopoverAnimated:YES];
    } else {
        [picker dismissViewControllerAnimated:YES completion:^{
            if (!self.imageView.image) {
                [self dismissViewControllerAnimated:NO completion:nil];
            }
        }];
    }
    
    if (!self.imageView.image) {
        _shouldDisappear = true;
    }
    
    self.isPresentingModal = NO;
}
- (void) harmonyEffect;
{    
    NSArray *commonColors = [self.cbImage getMostCommonColors:10 type:CBColorTypeAll];
    
    __unsafe_unretained HarmonyViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (isUnloading) return;
        
        [weakSelf.colorScroller.colors removeAllObjects];
        [weakSelf.colorScroller.colors addObjectsFromArray:commonColors];
        //show the colorview
        
        //NSLog(@"Found %i colors", [commonColors count]);
        [weakSelf.colorScroller refresh];
        
        weakSelf.cameraButton.frame = CGRectSetY(weakSelf.colorScroller.view.frame.origin.y - weakSelf.cameraButton.frame.size.height - 10, weakSelf.cameraButton.frame);
        
        weakSelf.libraryButton.frame = CGRectSetY(weakSelf.colorScroller.view.frame.origin.y - weakSelf.libraryButton.frame.size.height - 10, weakSelf.libraryButton.frame);
    });
}

- (void)showCamera:(BOOL)animate;
{
    self.isPresentingModal = YES;
    
    if (HAS_CAMERA) {
        
        self.isCameraMode = YES;
        
        //self.cameraImageView.hidden = YES;
        self.camera.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.camera.allowsEditing = NO;
        self.camera.showsCameraControls = YES;
        //self.camera.cameraOverlayView = self.cameraOverlay;
        //[self.camera.view addSubview:self.cameraOverlay];
        
        [self showButtons];
        
        [self showPicker:animate];
        
    } else {
        [self showLibrary:animate];
    }
}

- (void)showLibrary:(BOOL)animate;
{
    self.isPresentingModal = YES;
    
    self.isCameraMode = NO;
    
    [self showButtons];
    
    self.camera.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self showPicker:animate];
}

- (IBAction)categoryClicked:(id)sender {
    self.categoryPickerView.hidden = NO;
}

- (IBAction)categoryOkClicked:(id)sender {
    
    self.selectedBrand = [self pickerBrand];
    if (self.selectedBrand) {
        int row = [self.categoryPicker selectedRowInComponent:1];
        if (row > 0) {
            self.selectedCategory = [self colorCategoryForRow:row - 1 brand:self.selectedBrand];
        } else {
            self.selectedCategory = nil;
        }
    }
    else {
        self.selectedCategory = nil;
    }
    self.categoryPickerView.hidden = YES;
    [self updateSelectedLabel];
}

- (IBAction)categoryCancelClicked:(id)sender {
    
    self.categoryPickerView.hidden = YES;
}

- (void) showPicker:(BOOL)animate
{
    if (self.camera.sourceType == UIImagePickerControllerSourceTypePhotoLibrary && IS_IPAD) {
        self.popover = [[UIPopoverController alloc] initWithContentViewController:self.camera];
        self.popover.delegate=self;
        CGRect centerRect = CGRectCenter(CGRectMake(1, 1, 1, 1), self.view.frame);
        [self.popover presentPopoverFromRect:centerRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        [self presentModalViewController:self.camera animated:animate];
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController;
{
    return YES;
}

/* Called on the delegate when the user has taken action to dismiss the popover. This is not called when -dismissPopoverAnimated: is called directly.
 */
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController;
{
    
}

- (void) showButtons
{
    
}

- (void)viewDidUnload {
    [self setImageView:nil];
    [self setColorView:nil];
    [self setColorLabel:nil];
    [self setLibraryButton:nil];
    [self setInfoView:nil];
    [self setTopControls:nil];
    [self setCategoryButton:nil];
    [self setCategoryPicker:nil];
    [self setCategoryPickerView:nil];
    [self setCategoryView:nil];
    [super viewDidUnload];
}

- (IBAction)cameraClicked:(id)sender {
    [self showCamera:YES];
}

- (IBAction)libraryClicked:(id)sender {
    [self showLibrary:YES];
}

- (void)ColorScroller_titleClicked:(ColorScroller *)colorScroller;
{
    
}

- (void)ColorScroller_colorClicked:(ColorScroller *)colorScroller color:(UIColor*)uiColor;
{
    self.colorView.backgroundColor = uiColor;

    __unsafe_unretained HarmonyViewController *weakSelf = self;
    if (self.delegate && [self.delegate respondsToSelector:@selector(SelectColor_PickedColor:uiColor:)]) {
        //[SVProgressHUD showWithStatus:@"Locating Closest Match"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            if (isUnloading) return;
            Color *color = [PainterUtility closestMatchForUIColor:uiColor
                                                            brand:weakSelf.selectedBrand
                                                         category:weakSelf.selectedCategory];
            NSLog(@"Closest color is %@", color.name);
            dispatch_async(dispatch_get_main_queue(), ^{
                if (isUnloading) return;
                //[SVProgressHUD dismiss];
                weakSelf.color = color;
            });
        });
        
    }
}

- (void) setColor:(Color *)color
{
    _color = color;
    if (color) {
        self.colorView.backgroundColor = color.uiColor;
        if (IS_IPAD) {
            self.colorLabel.text = [NSString stringWithFormat:@"%@ - %@ - %@",
                                    color.category.brand.name,
                                    color.category.name,
                                    color.name];
        } else {
            self.colorLabel.text = [NSString stringWithFormat:@"%@ - %@",
                                    color.name,
                                    self.selectedBrand ? color.category.name : color.category.brand.name];
            //self.colorLabel.text = color.name;
        }
        self.colorLabel.hidden = NO;
    } else {
        self.colorLabel.hidden = YES;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    self.infoView.hidden = YES;
    
    [self getSampleColor:touch];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    [self getSampleColor:touch];
}

- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event
{
    self.imageDropper.hidden = YES;
}

- (void) getSampleColor:(UITouch *)touch
{
    if (!self.cbImage || !self.imageView.image) return;
    
    CGPoint touchPoint = [touch locationInView:self.imageView];
    
    self.imageDropper.hidden = YES;
    
    if (touchPoint.x < self.imageView.frame.size.height) {
        
        float widthScale = self.imageView.image.size.width / self.imageView.bounds.size.width;
        float heightScale = self.imageView.image.size.height / self.imageView.bounds.size.height;
        
        float scale;
        int xBorder = 0;
        int yBorder = 0;

        if (widthScale > heightScale) {
            scale = widthScale;
            yBorder = (self.imageView.image.size.height / heightScale - self.imageView.image.size.height / scale) / 2;
        } else {
            scale = heightScale;
            xBorder = (self.imageView.image.size.width / widthScale - self.imageView.image.size.width / scale) / 2;
        }
        
        CGPoint touchPointScaled = CGPointMake((touchPoint.x - xBorder) * scale,
                                               (touchPoint.y - yBorder) * scale);
        
        if (touchPointScaled.x < 0 || touchPointScaled.x >= self.cbImage.canvasSize.width
            || touchPointScaled.y < 0 || touchPointScaled.y >= self.cbImage.canvasSize.height) {
            return;
        }
        
        UIColor * uiColor = [self.cbImage.topLayer getColorAtPoint:touchPointScaled withRadius:10];
        
        if (!uiColor) return;
        
        double desiredX = touchPoint.x + 25;
        if (desiredX + self.imageDropper.frame.size.width > self.imageView.frame.size.width) {
            desiredX = touchPoint.x - self.imageDropper.frame.size.width - 25;
            self.imageDropperIcon.transform = CGAffineTransformMakeScale(-1, 1);
        } else {
            self.imageDropperIcon.transform = CGAffineTransformMakeScale(1, 1);
        }
        
        self.imageDropper.frame = CGRectSetX(desiredX, self.imageDropper.frame);
        self.imageDropper.frame = CGRectSetY(touchPoint.y - 85, self.imageDropper.frame);
        self.imageDropper.hidden = NO;
        self.imageDropperColor.color = uiColor;
        
        
        if (self.finderCount < 3) {
            self.finderCount ++;
            __unsafe_unretained HarmonyViewController *weakSelf = self;
            dispatch_sync(_finder_queue, ^{
                if (isUnloading) return;
                Color *color = [PainterUtility closestMatchForUIColor:uiColor
                                                                brand:weakSelf.selectedBrand
                                                             category:weakSelf.selectedCategory];
                //NSLog(@"Closest color is %@", color.name);
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (isUnloading) return;
                    weakSelf.color = color;
                    weakSelf.imageDropperColor.color = color.uiColor;
                    weakSelf.finderCount --;
                });
            });
        }
    }
}

- (Brand *) pickerBrand
{
    int selectedIndex = [self.categoryPicker selectedRowInComponent:0];
    if (selectedIndex > 0) {
        return [_brands objectAtIndex:selectedIndex - 1];
    } else {
        return nil;
    }
}

- (ColorCategory *)colorCategoryForRow:(int)row brand:(Brand *)brand
{
    int index = 0;
    for (ColorCategory *category in brand.categories) {
        if (index  == row) {
            return category;
        }
        index ++;
    }
    return nil;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component;
{
    if (component == 0) {
        return 0.4 * self.categoryPicker.frame.size.width;
    } else {
        return 0.6 * self.categoryPicker.frame.size.width;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component;
{
    return 30;
}

- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component
           reusingView:(UIView *)view;
{
    if (!view) {
        CGFloat width = [self pickerView:pickerView widthForComponent:component];
        CGFloat height = [self pickerView:pickerView rowHeightForComponent:component];
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    }
    
    
    UILabel * label = (UILabel *)[view viewWithTag:1];
    
    if (!label) {
        label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0,
                                                          view.frame.size.width - 10,
                                                          view.frame.size.height)];
        label.tag = 1;
        label.opaque = NO;
        label.backgroundColor = [UIColor clearColor];
    }

    if (component == 0) {
        label.font = IS_IPAD ? [UIFont boldSystemFontOfSize:17] : [UIFont systemFontOfSize:14];
        
        if (row == 0) {
            label.text = @"All Brands";
        } else {
            Brand *brand = [_brands objectAtIndex:row - 1];
            label.text = brand.name;
        }
        
    } else {
        Brand *selectedBrand = [self pickerBrand];
        
        if (selectedBrand) {
            label.font = IS_IPAD ? [UIFont boldSystemFontOfSize:17] : [UIFont systemFontOfSize:12];
            if (row == 0) {
                label.text = @"All Colors";
            } else {
                ColorCategory *category = [self colorCategoryForRow:row - 1 brand:selectedBrand];
                label.text = category.name;
            }
            
        } else {
            label.text = @"";
        }
    }
    
    [view addSubview:label];
    return view;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{
    if (component == 0) {
        [self.categoryPicker selectRow:0 inComponent:1 animated:NO];
        [self.categoryPicker reloadComponent:1];
    }
}

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 2;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    if (component == 0) {
        return [_brands count] + 1;
    } else {
        Brand *selectedBrand = [self pickerBrand];
        if (selectedBrand) {
            return [selectedBrand.categories count] + 1;
        }
        return 1;
    }
}

@end
