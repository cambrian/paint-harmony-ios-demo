//
//  HarmonyViewController.h
//  VirtualPainter
//
//  Created by Joel Teply on 11/12/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Controls.h"
#import "ColorScroller.h"
#import "SubViewController.h"

@interface HarmonyViewController : SubViewController
    <UIImagePickerControllerDelegate, UINavigationControllerDelegate,
    UIPopoverControllerDelegate, ColorScrollerDelegate,
    UIPickerViewDelegate, UIPickerViewDataSource>

@property (unsafe_unretained, nonatomic) IBOutlet UIView *topControls;
@property(assign) id<SelectColorDelegate> delegate;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *cameraButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *libraryButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imageView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *colorView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *colorLabel;
@property (strong, nonatomic) Color *color;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *infoView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *categoryView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *categoryButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIPickerView *categoryPicker;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *categoryPickerView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *toolbar;

- (IBAction)backClicked:(id)sender;

- (void)showCamera:(BOOL)animate;
- (void)showLibrary:(BOOL)animate;
- (IBAction)categoryClicked:(id)sender;
- (IBAction)categoryOkClicked:(id)sender;
- (IBAction)categoryCancelClicked:(id)sender;

- (IBAction)cameraClicked:(id)sender;
- (IBAction)libraryClicked:(id)sender;

@end
