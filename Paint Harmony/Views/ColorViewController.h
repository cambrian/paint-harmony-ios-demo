//
//  ColorViewController.h
//  AugmentedRoom
//
//  Created by Joel Teply on 11/3/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorScroller.h"
#import "SubViewController.h"
#import <MessageUI/MessageUI.h>

@class Color;

@protocol ColorViewControllerDelegate <NSObject>

- (void)ColorViewController_colorClicked:(Color *)color;

@end

@interface ColorViewController : SubViewController <ColorScrollerDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *favoritesInstructions;
@property (retain, nonatomic) Color *color;
@property (unsafe_unretained, nonatomic) id<ColorViewControllerDelegate> delegate;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *brandLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *colorNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *colorNumberLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *favoriteButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIBarButtonItem *actionButton;

- (IBAction)doneClicked:(id)sender;
- (IBAction)favoriteClicked:(id)sender;
- (IBAction)actionClicked:(id)sender;

@end
