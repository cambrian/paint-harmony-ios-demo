//
//  BaseViewController.m
//  Home Decorator
//
//  Created by Joel Teply on 1/3/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"
#import "PainterUtility.h"

@interface BaseViewController()
{
    
}

@property (strong, nonatomic) NSString *baseNibName;

@end

@implementation BaseViewController

- (void)dealloc {
    NSLog(@"Dismissing %@", self.baseNibName);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self.baseNibName = NSStringFromClass([self class]);
    
    if (!nibNameOrNil) {
        if (IS_IPAD) {
            NSString *ipadFilename = [NSString stringWithFormat:@"%@_iPad", self.baseNibName];
            if ([[NSBundle mainBundle] pathForResource:ipadFilename ofType:@"nib"]) {
                nibNameOrNil = ipadFilename;
            }
            
        } else {
            NSString *iphoneFilename = [NSString stringWithFormat:@"%@_iPhone", self.baseNibName];
            if ([[NSBundle mainBundle] pathForResource:iphoneFilename ofType:@"nib"]) {
                nibNameOrNil = iphoneFilename;
            }
        }
    }
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    self.hideStatusBar = YES;
    
    if (self) {
        // Custom initialization
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_colorChangedNotification:) name:ColorChangedNotification object:nil];
    }
    
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return self.hideStatusBar;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    } else {
        [[UIApplication sharedApplication] setStatusBarHidden:self.hideStatusBar withAnimation:UIStatusBarAnimationNone];
    }
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void) _colorChangedNotification:(NSNotification *)notification;
{
    [self colorChangedNotification:notification.object];
}

- (void) colorChangedNotification:(id)sender;
{
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

@end
