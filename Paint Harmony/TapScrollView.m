//
//  TapScrollView.m
//  Wall Painter
//
//  Created by Joel Teply on 9/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TapScrollView.h"

@interface TapScrollView ()

@end


@implementation TapScrollView

@synthesize twoFingerScrolling = _twoFingerScrolling;

- (void) initialize;
{
    self.multipleTouchEnabled = YES;
    self.scrollEnabled = YES;
}

- (id)initWithFrame:(CGRect)frame 
{
    self = [super initWithFrame:frame];
    [self initialize];
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder;
{
    self = [super initWithCoder:aDecoder];
    [self initialize];
    return self;
}

- (void)setTwoFingerScrolling:(BOOL)twoFingerScrolling
{
    if (_twoFingerScrolling == twoFingerScrolling) return;
    
    _twoFingerScrolling = twoFingerScrolling;
    
    for (UIGestureRecognizer *gestureRecognizer in self.gestureRecognizers) {
        if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
            UIPanGestureRecognizer *panGR = (UIPanGestureRecognizer *) gestureRecognizer;
            panGR.minimumNumberOfTouches = twoFingerScrolling ? 2 : 1;
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	// If not dragging, send event to next responder
    //NSLog(@"touchesBegan");
    int fingerCount = [touches count];
    
    if (fingerCount == 1) {
        [((UIResponder *) self.delegate) touchesBegan:touches withEvent:event];
    } else {
        [super touchesBegan: touches withEvent: event];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	// If not dragging, send event to next responder
	
    //NSLog(@"touchesMoved");
    int fingerCount = [touches count];
    
    if (fingerCount == 1) {
        [((UIResponder *) self.delegate) touchesMoved:touches withEvent:event];
    } else {
        [super touchesMoved: touches withEvent: event];
    }
}

- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event
{
    //NSLog(@"touchesEnded");
    int fingerCount = [touches count];
	// If not dragging, send event to next responder
	if (fingerCount <= 1) {
        //send to painter or whatever
        [((UIResponder *) self.delegate) touchesEnded:touches withEvent:event];
		//[self.nextResponder touchesEnded: touches withEvent:event];
    }
	else {
		[super touchesEnded: touches withEvent: event];
    }
}

- (void) touchesCancelled: (NSSet *) touches withEvent: (UIEvent *) event
{
    //NSLog(@"touchesCancelled");
    [((UIResponder *) self.delegate) touchesCancelled:touches withEvent:event];
}

@end
