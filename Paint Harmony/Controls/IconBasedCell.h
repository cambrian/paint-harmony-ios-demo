//
//  ColorCategoryCell.h
//  AugmentedRoom
//
//  Created by Joel Teply on 10/23/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface IconBasedCell : UITableViewCell

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *bgView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *iconImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *headingLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *subheadingLabel;

@end
