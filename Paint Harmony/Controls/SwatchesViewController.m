//
//  SwatchesViewController.m
//  Home Decorator
//
//  Created by Joel Teply on 1/3/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import "SwatchesViewController.h"
#import "PaintSwatch.h"
#import "Color+Extended.h"
#import "TapScrollView.h"
#import "Brand.h"

#define SWATCH_WIDTH (IS_IPAD ? 60 : 30)
#define SWATCH_HEIGHT SWATCH_WIDTH
#define SWATCH_SPACING 4

@interface SwatchesViewController () {
    int _totalRows;
}

@property (nonatomic, retain) __block NSMutableArray *swatches;
@property (nonatomic, retain) __block PaintSwatch *selectedSwatch;
@property (nonatomic, assign) enum ColorType colorType;

@end

@implementation SwatchesViewController

@synthesize scrollView = _scrollView;
@synthesize colorView = _colorView;
@synthesize delegate = _delegate;
@synthesize colorCategory = _colorCategory;
@synthesize swatches = _swatches;
@synthesize selectedColor = _selectedColor;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.swatches = [NSMutableArray array];
    
    _totalRows = 4;
}

- (void) setColorCategory:(ColorCategory *)colorCategory
{
    _colorCategory = colorCategory;
    
    [self refresh];
}

- (NSArray *)allBehrColors; {
    Brand *behr = [[CBCoreData sharedInstance] getFirstRecordForClass:[Brand class]
                                                             predicate:[NSPredicate predicateWithFormat:@"name==%@", @"Behr"]];

    return [self allBrandColors:behr];
}

- (NSArray *)allColors; {
    NSArray *filteredColors = [[CBCoreData sharedInstance] getRecordsForClass:[Color class] predicate:nil];
    
    return filteredColors;
}

- (NSArray *)allBrandColors:(Brand *)brand {

    NSMutableArray *filteredColors = [NSMutableArray array];
    for (ColorCategory *category in brand.categories) {
        for (Color *color in [category.colors allObjects]) {
            [filteredColors addObject:color];
        }
    }
    return filteredColors;
}

- (NSArray *)filteredColorsByType:(enum ColorType)type {
    
    if (type == ColorTypeUndefined) {
        return [self allBehrColors];
    }
    
    NSArray *allColors = [self allBehrColors];
    
    NSMutableArray *filteredColors = [NSMutableArray array];
    for (Color *color in allColors) {
        if (color.colorType == type) {
            [filteredColors addObject:color];
        }
    }

    return filteredColors;
}

-(IBAction)captureScreen
{
    static BOOL hasCaptured = NO;
    
    if (hasCaptured) return;
    
    hasCaptured = YES;
    CGSize size = CGSizeMake(self.scrollView.contentSize.width, self.colorView.frame.size.height);
    //NSLog(@"Size: %f, %f", size.width, size.height);
    UIGraphicsBeginImageContext(size);
    [self.colorView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *name = [NSString stringWithFormat:@"%@.png", [Color colorTypeToString:self.colorType]];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:name];
    NSData *imageData = UIImagePNGRepresentation(viewImage);
    [imageData writeToFile:savedImagePath atomically:NO];
}

- (void) refresh;
{
    int height = _totalRows * (SWATCH_HEIGHT + SWATCH_SPACING);
    
    //remove all existing
    for (PaintSwatch *swatch in self.swatches) {
        [swatch removeFromSuperview];
    }
    [self.swatches removeAllObjects];
    
    int row = 0;
    int col = 0;
    
    self.colorType = ColorTypeUndefined;
    
    self.colorType = ColorTypePurple;
    
    NSArray *sorted = [[self filteredColorsByType:self.colorType] sortedArrayUsingComparator: ^NSComparisonResult(Color *colorA, Color *colorB) {
        CGFloat rgbA[3];
        [colorA.uiColor getRed:&rgbA[0] green:&rgbA[1] blue:&rgbA[2] alpha:nil];
        
        CGFloat rgbB[3];
        [colorB.uiColor getRed:&rgbB[0] green:&rgbB[1] blue:&rgbB[2] alpha:nil];
        
        double intensityA = 0.299 * rgbA[0] + 0.587 * rgbA[1] + 0.114 * rgbA[2];
        double intensityB = 0.299 * rgbB[0] + 0.587 * rgbB[1] + 0.114 * rgbB[2];

        if (colorA.colorType == colorB.colorType) {
            return intensityA < intensityB;
        } else {
            return colorA.colorType < colorB.colorType;
        }
        
        //return [colorA.color_id intValue] > [colorB.color_id intValue];
    }];
    
    for (Color *color in sorted) {
        int x = (SWATCH_WIDTH + SWATCH_SPACING) * col;
        int y = (SWATCH_HEIGHT + SWATCH_SPACING) * row;
        PaintSwatch *swatch = [[PaintSwatch alloc] initWithFrame:CGRectMake(x, y, SWATCH_WIDTH, SWATCH_HEIGHT)];
        swatch.color = color;
        swatch.selected = self.selectedColor == swatch.color;
        if (swatch.selected) {
            self.selectedSwatch = swatch;
        }
        
        row ++;
        if (row == _totalRows) {
            row = 0;
            col ++;
        }
        
        [self.swatches addObject:swatch];
    }
    
    int maxX = CGRectGetMaxX([[self.swatches lastObject] frame]);
    int maxY = CGRectGetMaxY([[self.swatches lastObject] frame]);
    
    __unsafe_unretained SwatchesViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        for (PaintSwatch *swatch in weakSelf.swatches) {
            [weakSelf.colorView addSubview:swatch];
        }
        
        weakSelf.scrollView.contentSize = CGSizeMake(maxX, maxY);
        [weakSelf selectedSwatchChanged];
        [self captureScreen];
    });
    
    self.view.frame = CGRectSetH(height, self.view.frame);
    
    
    //NSLog(@"w=%f,%f,%f", self.view.frame.size.width, self.view.bounds.size.width, self.scrollView.bounds.size.width);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) selectedSwatchChanged;
{
    if (self.selectedSwatch) {
        [self.scrollView scrollRectToVisible:self.selectedSwatch.frame animated:NO];
    } else {
        [self.scrollView scrollRectToVisible:CGRectZero animated:NO];
    }
}

#pragma mark -
#pragma mark ScrollView

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return self.colorView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	//self.colorHighlighter.hidden = YES;
}

#pragma mark -
#pragma mark Touch Events and Buttons

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //cancel bubble up
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;
{
	
	//Get all the touches.
	NSSet *allTouches = [event allTouches];
	
	//Get the first touch.
	UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
	
	if([touch tapCount] == 1) {
		int swatchXDistance = SWATCH_SPACING + SWATCH_WIDTH;
		int swatchYDistance = SWATCH_SPACING + SWATCH_HEIGHT;
		CGPoint loc = [touch locationInView: self.colorView];
        
		int col = loc.x / swatchXDistance;
		int row = loc.y / swatchYDistance;
		
		if (row > _totalRows) return;
        
        int index = col * _totalRows + row;
        
        if (index >= [self.swatches count]) return;
        
		PaintSwatch *clickedSwatch = [self.swatches objectAtIndex:index];
        clickedSwatch.selected = YES;
        
        if (self.selectedSwatch != clickedSwatch) {
            self.selectedSwatch.selected = false;
        }
        self.selectedSwatch = clickedSwatch;
		
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(SelectColor_PickedColor:uiColor:)]) {
            [self.delegate SelectColor_PickedColor:clickedSwatch.color uiColor:clickedSwatch.backgroundColor];
        }
	}
}

- (void) setSelectedColor:(Color *)selectedColor;
{
    if (selectedColor == _selectedColor) {
        return;
    }
    
    _selectedColor = selectedColor;
    
    __unsafe_unretained SwatchesViewController * weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (weakSelf.selectedSwatch) {
            weakSelf.selectedSwatch.selected = NO;
            weakSelf.selectedSwatch = nil;
        }
        
        for (PaintSwatch *swatch in weakSelf.swatches) {
            if (swatch.color == selectedColor) {
                swatch.selected = YES;
                weakSelf.selectedSwatch = swatch;
                break;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf selectedSwatchChanged];
        });
    });
}

@end
