//
//  ColorCategoryCell.m
//  AugmentedRoom
//
//  Created by Joel Teply on 10/23/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "IconBasedCell.h"
#import "Controls.h"

@interface IconBasedCell()

@end

@implementation IconBasedCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    self.headingLabel.textColor = UICOLOR_SIDEBAR_ITEM_TEXT;
    self.subheadingLabel.textColor = UICOLOR_SIDEBAR_ITEM_TEXT;
    
    if (!cellBgImage) cellBgImage = [UIImage imageNamed:@"item-bg.png"];
    if (!cellHighlightedBGImage) cellHighlightedBGImage = [UIImage imageNamed:@"item-bg-on.png"];
    
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (self.isHighlighted == highlighted) return;
    
    [super setHighlighted:highlighted animated:animated];
    
    __unsafe_unretained IconBasedCell *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.bgView.image = highlighted ? cellHighlightedBGImage : cellBgImage;
    });
}

@end
