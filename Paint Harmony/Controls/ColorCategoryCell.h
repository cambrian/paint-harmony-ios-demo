//
//  ColorCategoryCell.h
//  AugmentedRoom
//
//  Created by Joel Teply on 10/23/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ColorCategory;

@interface ColorCategoryCell : UITableViewCell

@property (strong, nonatomic) ColorCategory *colorCategory;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *bgView;

@end
