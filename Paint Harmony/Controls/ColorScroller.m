//
//  ColorScroller.m
//  VirtualPainter
//
//  Created by Joel Teply on 11/7/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "ColorScroller.h"
#import "PainterUtility.h"
#import "Color.h"
#import "Color+Extended.h"
#import "TapScrollView.h"

@interface ColorScrollerButton : UIButton

@property (assign, nonatomic) BOOL selected;

@end

@implementation ColorScrollerButton

@synthesize selected = _selected;

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect;
{
    [super drawRect:rect];
    
    if (self.selected) {
        //more stuff
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetLineWidth(context, IS_IPAD ? 6 : 4);
        CGContextSetLineCap(context, kCGLineCapRound);
        CGContextSetLineJoin(context, kCGLineJoinRound);
        CGContextSetStrokeColorWithColor(context, [UIColor greenColor].CGColor);
        
        CGContextSetRGBStrokeColor(context, 0, 255, 0, 1);
        CGContextStrokeRect(context, rect);
        
    }
}

@end

@interface ColorScroller () {
    CGRect _nextToFrame;
    BOOL _isOpening;
    ColorScrollerButton *_selectedButton;
}

@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UIView *titleBGView;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIView *scrollViewContent;
@property (retain, nonatomic) IBOutlet UIView *scrollViewBG;
@property (retain, nonatomic) UIButton *titleButton;

@end

@implementation ColorScroller

@synthesize heading = _heading;
@synthesize colorViewSize = _colorViewSize;
@synthesize colors = _colors;
@synthesize texture = _texture;

- (id) init;
{
    self = [super init];
    _colors = [NSMutableArray array];
    return self;
}

- (id) initWithDelegate:(id <ColorScrollerDelegate>)delegate
                  title:(NSString *)title
                 colors:(NSArray *)colors
                texture:(UIImage *)texture
                   size:(CGSize)size;
{
    self = [self init];
    self.delegate = delegate;
    self.heading = title;
    if (colors) [self.colors addObjectsFromArray:colors];
    self.texture = texture;
    self.colorViewSize = size;
    return self;
}

#pragma mark -
#pragma mark Load
- (void)viewDidLoad
{
    [super viewDidLoad];

    
    self.view.frame = CGRectSetH(self.colorViewSize.height + self.titleBGView.frame.size.height, self.view.frame);
        
    [self refresh];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!self.titleButton) {
        self.titleButton = [[UIButton alloc] initWithFrame:self.titleBGView.frame];
        [self.titleButton addTarget:self action:@selector(titleClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:self.titleButton];
    }
}

- (void)titleClicked
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ColorScroller_titleClicked:)]) {
        [self.delegate ColorScroller_titleClicked:self];
    }
}

- (void)viewDidUnload {
    [self setTitleLabel:nil];
    [self setScrollView:nil];
    [self setTitleBGView:nil];
    [self setScrollViewContent:nil];
    [self setArrowImage:nil];
    [super viewDidUnload];
}

- (void)refresh;
{
    self.titleLabel.text = self.heading;
    self.titleLabel.hidden = !self.titleLabel.text;
    self.titleBGView.hidden = self.titleLabel.hidden;
    
    for (UIView *view in self.scrollViewContent.subviews) {
        [view removeFromSuperview];
    }
    
    int spacing = 1;
    CGSize colorViewSize = self.colorViewSize;
    
//    if (colorViewSize.width * [self.colors count] < self.scrollView.frame.size.width) {
//        colorViewSize = CGSizeMake(self.scrollView.frame.size.width / self.colors.count - spacing,
//                                   colorViewSize.height);
//    }
    
    float width = 0;
    for (int i = 0; i < [self.colors count]; i++) {
        id color = [self.colors objectAtIndex:i];
        
        UIColor *uiColor;
        if ([color isKindOfClass:[UIColor class]]) {
            uiColor = color;
        } else {
            uiColor = [color uiColor];
        }
                
        CGRect frame = CGRectMake((colorViewSize.width + spacing) * i,
                                  0,
                                  colorViewSize.width,
                                  colorViewSize.height);
        
        UIView *aView = [PainterUtility makeColorTexture:self.texture withColor:uiColor withFrame:frame];
        //UIView *aView = [[UIView alloc] initWithFrame:frame];
        aView.backgroundColor = uiColor;
        aView.opaque = YES;
        [self.scrollViewContent addSubview:aView];
        UIImageView *arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"swatch_details.png"]];
        arrowView.frame = CGRectCenterY(arrowView.frame, aView.frame);
        arrowView.frame = CGRectSetX(aView.frame.size.width - arrowView.frame.size.width, arrowView.frame);
        [aView addSubview:arrowView];
        ColorScrollerButton *aViewButton = [[ColorScrollerButton alloc] initWithFrame:aView.frame];
        aViewButton.tag = i;
        [aViewButton addTarget:self action:@selector(colorClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollViewContent addSubview:aViewButton];
        width = CGRectGetMaxX(frame);
    }
    
    self.scrollView.frame = CGRectSetH(self.colorViewSize.height, self.scrollView.frame);
    
    if (self.titleLabel.hidden) {
        self.scrollView.frame = CGRectSetY(0, self.scrollView.frame);
    }
    self.scrollViewBG.frame = self.scrollView.frame;
    
    self.scrollViewContent.frame = CGRectSetSize(CGSizeMake(width, self.colorViewSize.height),
                                                 self.scrollViewContent.frame);
    
    self.scrollView.contentSize = CGSizeMake(width, self.scrollViewContent.frame.size.height);
    self.view.frame = CGRectSetH(CGRectGetMaxY(self.scrollView.frame), self.view.frame);
}

#pragma mark -
#pragma mark Private

- (void)colorClicked:(id)sender
{
    ColorScrollerButton *button = sender;
    
    if (_selectedButton) {
        _selectedButton.selected = NO;
    }
    _selectedButton = button;
    _selectedButton.selected = YES;
    id color = [self.colors objectAtIndex:button.tag];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(ColorScroller_colorClicked:color:)]) {
        [self.delegate ColorScroller_colorClicked:self color:color];
    }
}

#pragma mark -
#pragma mark ScrollView

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return self.scrollViewContent;
}

- (void)prepareForCollapse;
{
    BOOL isOpen = self.view.frame.size.height > self.titleBGView.frame.size.height;
    
    float height = 0;
    if (isOpen) {
        //close it
        _isOpening = NO;
        height = self.titleBGView.frame.size.height;
    } else {
        _isOpening = YES;
        height = self.titleBGView.frame.size.height + self.colorViewSize.height;
    }
    
    _nextToFrame = CGRectSetH(height, self.view.frame);
    
    float changeInHeight = self.view.frame.size.height - height;
    _nextToFrame = CGRectSetY(changeInHeight + self.view.frame.origin.y, _nextToFrame);
}

- (void)performCollapse;
{    
    self.view.frame = _nextToFrame;
    self.arrowImage.transform = _isOpening ? CGAffineTransformMakeRotation(0)
        : CGAffineTransformMakeRotation(M_PI / 2);
}

@end
