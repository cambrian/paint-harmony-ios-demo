//
//  ColorScroller.h
//  VirtualPainter
//
//  Created by Joel Teply on 11/7/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "BaseViewController.h"

@class ColorScroller;
@protocol ColorScrollerDelegate <NSObject>

- (void)ColorScroller_titleClicked:(ColorScroller *)colorScroller;
- (void)ColorScroller_colorClicked:(ColorScroller *)colorScroller color:(id)color;

@end

@interface ColorScroller : BaseViewController <UIScrollViewDelegate>

@property (assign, nonatomic) id <ColorScrollerDelegate> delegate;
@property (retain, nonatomic) NSString *heading;
@property (assign, nonatomic) CGSize colorViewSize;
@property (readonly, nonatomic) NSMutableArray *colors;
@property (retain, nonatomic) UIImage *texture;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *arrowImage;

- (id) initWithDelegate:(id <ColorScrollerDelegate>)delegate
                  title:(NSString *)title
                 colors:(NSArray *)colors
                texture:(UIImage *)texture
                   size:(CGSize)size;

- (void)prepareForCollapse;
- (void)performCollapse;
- (void)refresh;

@end
