//
//  ColorTabsViewController.h
//  Home Painter
//
//  Created by Joel Teply on 1/21/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "Controls.h"

typedef enum
{
    ColorTabNone,
    ColorTabHSV,
	ColorTabSwatches,
    ColorTabFavorites,
}  ColorTab;

@protocol ColorTabDelegate <SelectColorDelegate>

@optional
- (void)drawerButtonClicked;
- (void)tabOpened:(ColorTab)tab view:(UIView *)view;
- (void)tabClosed:(ColorTab)tab view:(UIView *)view;

@end

@class ColorCategory;

@interface ColorTabsViewController : BaseViewController <SelectColorDelegate>

//HSV
@property (weak, nonatomic) IBOutlet UIButton *drawerButton;
@property (assign, nonatomic) id<ColorTabDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *hsvColorSelector;
@property (weak, nonatomic) IBOutlet UIButton *hsvColorButton;
@property (weak, nonatomic) IBOutlet UISlider *hueSlider;
@property (weak, nonatomic) IBOutlet UITextField *hueTextbox;
@property (weak, nonatomic) IBOutlet UISlider *satSlider;
@property (weak, nonatomic) IBOutlet UITextField *satTextbox;
@property (weak, nonatomic) IBOutlet UISlider *valSlider;
@property (weak, nonatomic) IBOutlet UITextField *valTextbox;

@property (weak, nonatomic) IBOutlet UIButton *matchClosestCheckbox;
@property (weak, nonatomic) IBOutlet UIButton *brandCheckbox;
@property (weak, nonatomic) IBOutlet UIButton *categoryCheckbox;
//Swatches
@property (weak, nonatomic) IBOutlet UIView *swatchesSelector;
@property (weak, nonatomic) IBOutlet UIButton *swatchesButton;

//Theory
@property (weak, nonatomic) IBOutlet UIView *favoritesSelector;
@property (weak, nonatomic) IBOutlet UIButton *favoritesButton;
@property (weak, nonatomic) IBOutlet UIView *favoritesInstructions;
@property (assign, nonatomic) ColorTab currentTab;

@property (strong, nonatomic) ColorCategory *colorCategory;

- (IBAction)drawerClicked:(id)sender;
- (IBAction)colorChanged:(id)sender;
- (IBAction)hsvClicked;
- (IBAction)swatchesClicked;
- (IBAction)matchClosestCheckboxClicked:(id)sender;
- (IBAction)brandCheckboxClicked:(id)sender;
- (IBAction)categoryCheckboxClicked:(id)sender;

- (CGRect) closedFrame;

@end
