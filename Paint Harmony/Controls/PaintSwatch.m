//
//  PaintSwatch.m
//  Home Painter
//
//  Created by Joel Teply on 1/26/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import "PaintSwatch.h"
#import "Color+Extended.h"

@implementation PaintSwatch

@synthesize color = _color;
@synthesize selected = _selected;

- (void) setColor:(Color *)value;
{
    _color = value;
    self.backgroundColor = value.uiColor;
}

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect;
{
    [super drawRect:rect];
    
    if (self.selected) {
        //more stuff
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetLineWidth(context, IS_IPAD ? 6 : 4);
        CGContextSetLineCap(context, kCGLineCapRound);
        CGContextSetLineJoin(context, kCGLineJoinRound);
        CGContextSetStrokeColorWithColor(context, [UIColor greenColor].CGColor);
        CGContextStrokeRect(context, rect);
        
    }
}

@end
