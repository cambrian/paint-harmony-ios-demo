//
//  ColorCategoryCell.m
//  AugmentedRoom
//
//  Created by Joel Teply on 10/23/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "ColorCategoryCell.h"
#import "ColorCategory.h"
#import "Brand.h"
#import "Controls.h"

@interface ColorCategoryCell()

@property (retain, nonatomic) IBOutlet UILabel *brandLabel;
@property (retain, nonatomic) IBOutlet UILabel *categoryLabel;
@property (retain, nonatomic) IBOutlet UIImageView *brandIcon;

@end

@implementation ColorCategoryCell

@synthesize colorCategory = _colorCategory;
@synthesize brandIcon = _brandIcon;
@synthesize categoryLabel = _categoryLabel;
@synthesize brandLabel = _brandLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setColorCategory:(ColorCategory *)colorCategory;
{
    _colorCategory = colorCategory;
    
    self.brandLabel.text = colorCategory.brand.name;
    
    self.brandIcon.image = [UIImage imageNamed:colorCategory.brand.icon];
    self.categoryLabel.text = colorCategory.name;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    self.brandLabel.textColor = UICOLOR_SIDEBAR_ITEM_TEXT;
    self.categoryLabel.textColor = UICOLOR_SIDEBAR_ITEM_TEXT;
    
    if (!cellBgImage) cellBgImage = [UIImage imageNamed:@"item-bg.png"];
    if (!cellHighlightedBGImage) cellHighlightedBGImage = [UIImage imageNamed:@"item-bg-on.png"];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (self.isHighlighted == highlighted) return;
    
    [super setHighlighted:highlighted animated:animated];
    
    __unsafe_unretained ColorCategoryCell *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.bgView.image = highlighted ? cellHighlightedBGImage : cellBgImage;
    });
}

@end
