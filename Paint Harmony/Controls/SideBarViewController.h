//
//  SideBarViewController.h
//  AugmentedRoom
//
//  Created by Joel Teply on 10/15/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@class ColorCategory, Color;

@protocol SideBarDelegate <NSObject>

@required

- (void) SideBar_ShowLibrary;
- (void) SideBar_PickedColorCategory:(ColorCategory *)colorCategory;
- (void) SideBar_OpenedColorPicker;
- (void) SideBar_OpenedHarmony:(BOOL)isCamera;
- (void) SideBar_ShowFavorites;
- (void) SideBar_PickedColor:(Color *)color;
- (void) SideBar_EmailMore;
- (void) SideBar_LoadImage:(UIImage *)image;

@end

@interface SideBarViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (assign) id<SideBarDelegate> delegate;
@property (retain, nonatomic) IBOutlet UITextField *searchText;
@property (retain, nonatomic) IBOutlet UITableView *tableView;

- (void)sidebarWillAppear;
- (void)sidebarWillDisappear;

@end