//
//  SideBarViewController.m
//  AugmentedRoom
//
//  Created by Joel Teply on 10/15/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "SideBarViewController.h"
#import "ColorCategory.h"
#import "ColorCategoryCell.h"
#import "MoreCell.h"
#import "IconBasedCell.h"
#import "PainterUtility.h"
#import "Brand.h"
#import "Color.h"
#import "Color+Extended.h"

#define MAX_INITIAL_CATEGORIES 5
#define FEATURES @"Features"
#define FEATURES_LIBRARY @"Load photo from library"
#define FEATURES_COLOR_FAVORITES @"Favorites"
#define FEATURES_COLOR_CAMERA @"Color From Camera"
#define FEATURES_COLOR_LIBRARY @"Color From Photo Library"
#define FEATURES_COLOR_PICKER @"Color Picker"
#define FEATURES_EXAMPLE @"Load Example Image"
#define FEATURES_COMPLEX_EXAMPLE @"Load Complex Image"

#define COLOR_MORE @"More Colors"

@interface SideBarViewController () {
    NSMutableArray *_headings;
    int _expandedIndex;
    UIView *_expandedArrow;
    NSMutableDictionary * _colorBrands;
    
    NSMutableArray *_theoryHeadings;
    NSMutableArray *_theorySubHeadings;
    NSMutableArray *_theoryIcons;
    
    NSMutableArray *_searchResults;
    dispatch_source_t _searchTimer;
}

@property (assign, nonatomic) BOOL searchMode;
@property (retain, nonatomic) UIView *searchScreen;
@property (retain, nonatomic) UIActivityIndicatorView * loadingIndicator;
@property (retain, nonatomic) UILabel * loadingIndicatorLabel;

@end

@implementation SideBarViewController

@synthesize delegate = _delegate;
@synthesize searchText = _searchText;
@synthesize tableView = _tableView;
@synthesize searchMode = _searchMode;
@synthesize searchScreen = _searchScreen;
@synthesize loadingIndicator = _loadingIndicator;
@synthesize loadingIndicatorLabel = _loadingIndicatorLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self loadData];
        _expandedIndex = -1;
        self.view.backgroundColor = UICOLOR_SIDEBAR_BG;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchTextChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    }
    return self;
}

- (void)sidebarWillAppear;
{
    BOOL wasSearchMode = self.searchMode;
    
    self.searchScreen.frame  = self.tableView.frame;
    
    self.searchMode = NO;
    [_searchResults removeAllObjects];
    self.searchText.text = nil;
    [self.tableView setContentOffset:CGPointZero animated:NO];
    
    if (wasSearchMode != self.searchMode) {
        [self.tableView reloadData];
    }
}

- (void)sidebarWillDisappear;
{
    self.searchText.text = nil;
    self.searchScreen.hidden = YES;
    [self.searchText resignFirstResponder];
    [self searchTextChanged:self];
}

- (void) loadData
{
    _searchResults = [NSMutableArray array];
    _headings = [NSMutableArray array];
    [_headings addObject:FEATURES];
    
    _theoryHeadings = [NSMutableArray array];
    _theoryIcons = [NSMutableArray array];
    _theorySubHeadings = [NSMutableArray array];
    
    [self addMainSection:FEATURES_LIBRARY
              subHeading:@"Load a photo from your library"
                    icon:@"library_cell_icon.png"];

//    [self addMainSection:FEATURES_COLOR_FAVORITES
//              subHeading:@"Pick a color from your favorites"
//                    icon:@"favorites_cell_icon.png"];

    [self addMainSection:FEATURES_COLOR_CAMERA
              subHeading:@"Get a color using the camera"
                    icon:@"camera_cell_icon.png"];
    
    [self addMainSection:FEATURES_COLOR_LIBRARY
              subHeading:@"Get a color from a photo in your library"
                    icon:@"color_library_cell_icon.png"];
    
    [self addMainSection:FEATURES_EXAMPLE
              subHeading:@"Load the default image"
                    icon:INITIAL_IMAGE];

    [self addMainSection:FEATURES_COMPLEX_EXAMPLE
              subHeading:@"Load complex image"
                    icon:@"lattice_thumbnail.png"];
    
//    [self addMainSection:FEATURES_COLOR_PICKER
//              subHeading:@"Choose color values directly"
//                    icon:@"color_wheel_cell.png"];
    
    NSArray *brands = [PainterUtility allBrands];
    NSLog(@"Got %i brands", [brands count]);
    
    _colorBrands = [NSMutableDictionary dictionary];
    for (Brand *brand in brands) {
        [_headings addObject:[brand.name uppercaseString]];
        [_colorBrands setObject:brand forKey:[brand.name uppercaseString]];
    }

//    //projects, settings, less important things
    [_headings addObject:COLOR_MORE];
//    [_headings addObject:@"Settings"];
}

- (void)addMainSection:(NSString *)heading subHeading:(NSString *)subHeading icon:(NSString *)imageName
{
    UIImage *icon = [UIImage imageNamed:imageName];
    if (!icon) {
        NSLog(@"Icon %@ not found", imageName);
        return;
    }
    [_theoryHeadings addObject:heading];
    [_theorySubHeadings addObject:subHeading];
    [_theoryIcons addObject:icon];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.searchScreen  = [[UIView alloc] initWithFrame:self.tableView.frame];
    self.searchScreen.hidden = YES;
    self.searchScreen.backgroundColor = UICOLOR_SIDEBAR_BG;
    self.searchScreen.alpha = 0.5;
    
    if ([self.searchText valueForKeyPath:@"_placeholderLabel.textColor"]) {
        [self.searchText setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    }
    
    self.loadingIndicatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 100, 20)];
    self.loadingIndicatorLabel.text = @"Loading";
    self.loadingIndicatorLabel.backgroundColor = [UIColor clearColor];
    self.loadingIndicatorLabel.opaque = NO;
    self.loadingIndicatorLabel.textColor = [UIColor darkTextColor];
    self.loadingIndicatorLabel.frame = CGRectCenterX(self.loadingIndicatorLabel.frame, self.searchScreen.frame);
    self.loadingIndicatorLabel.frame = CGRectSetX(self.loadingIndicatorLabel.frame.origin.x + 20,
                                                  self.loadingIndicatorLabel.frame);
    self.loadingIndicatorLabel.hidden = YES;
    [self.searchScreen addSubview:self.loadingIndicatorLabel];
    
    self.loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.loadingIndicator.frame = CGRectSetY(5, self.loadingIndicator.frame);
    self.loadingIndicator.frame = CGRectSetX(self.loadingIndicatorLabel.frame.origin.x
                                             - self.loadingIndicator.frame.size.width - 5, self.loadingIndicator.frame);
    [self.searchScreen addSubview:self.loadingIndicator];
    
    [self.view insertSubview:self.searchScreen aboveSubview:self.tableView];
}

#pragma mark @protocol Text Field Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    self.searchMode = YES;
    self.searchScreen.hidden = NO;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text
{
    
    return YES;
}

- (void)searchTextChanged:(id)sender {
    if (self.searchText.text.length > 0) {
        [self searchQuery:self.searchText.text];
    } else {
        [_searchResults removeAllObjects];
        [self.loadingIndicator stopAnimating];
        self.loadingIndicatorLabel.hidden = YES;
        [self.tableView reloadData];
    }
}

- (void) searchQuery:(NSString *)text
{
    __unsafe_unretained SideBarViewController * weakSelf = self;
    
    self.searchScreen.hidden = NO;
    [self.loadingIndicator startAnimating];
    self.loadingIndicatorLabel.hidden = NO;
    
    NSScanner *scanner = [NSScanner scannerWithString:text];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    // Throw away characters before the first number.
    [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
    
    // Collect numbers.
    NSString *numbersString;
    [scanner scanCharactersFromSet:numbers intoString:&numbersString];
    
    [CBThreading performBlock:^{
        //execute lookup:
        NSMutableString *query = [numbersString length] ? [NSMutableString stringWithString:@"(code CONTAINS[cd] %@)"] : [NSMutableString stringWithString:@"(name CONTAINS[cd] %@)"];

        NSPredicate *predicate = [NSPredicate predicateWithFormat:query, text];
        _searchResults = [NSMutableArray  arrayWithArray:[[CBCoreData sharedInstance] getRecordsForClass:[Color class] predicate:predicate sortedBy:nil context:nil]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //show loading
            [weakSelf.tableView reloadData];
            
            weakSelf.searchScreen.hidden = YES;
            weakSelf.loadingIndicatorLabel.hidden = YES;
            [weakSelf.loadingIndicator stopAnimating];
        });
    } onQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
      withIdentifier:@"searchQuery"];
}

#pragma mark @protocol UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    if (self.searchMode) {
        return 1;
    }
    return [_headings count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
{
    if (self.searchMode) {
        return @"Search Results";
    }
    return [[_headings objectAtIndex:section] uppercaseString];
}

- (int) numberOfRowsInSection:(int)section
{
    NSString *sectionName = [self tableView:self.tableView titleForHeaderInSection:section];
    if ([sectionName isEqualToString:[FEATURES uppercaseString]]) {
        return [_theoryHeadings count];
    }
    else if ([sectionName isEqualToString:[COLOR_MORE uppercaseString]]) {
        return 1;
    }
    else {
        Brand *brand = [_colorBrands objectForKey:sectionName];
        if (brand) {
            return [brand.categories count];
        }
    }

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (self.searchMode) {
        return [_searchResults count];//search results length
    }
    
    int number = [self numberOfRowsInSection:section];
    
    NSString *sectionName = [self tableView:tableView titleForHeaderInSection:section];
    Brand *brand = [_colorBrands objectForKey:sectionName];
    
    if (brand && number > MAX_INITIAL_CATEGORIES && _expandedIndex != section) {
        number = MAX_INITIAL_CATEGORIES + 1;
    }
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (self.searchMode) {
        static NSString *iconCellNib = @"IconBasedCell";
        
        IconBasedCell *iconCell = [self getCellWithNib:iconCellNib
                                               ofClass:[IconBasedCell class]
                                             tableView:tableView];
        Color *searchResult = [_searchResults objectAtIndex:indexPath.row];
        iconCell.headingLabel.text = searchResult.category.brand.name;
        iconCell.subheadingLabel.text = [NSString stringWithFormat:@"%@ #%@", searchResult.name, searchResult.code];
        iconCell.iconImageView.backgroundColor = [searchResult uiColor];
        
        return iconCell;//search results length
    }
    
    NSString *sectionName = [self tableView:tableView titleForHeaderInSection:indexPath.section];
    
    Brand *brand = [_colorBrands objectForKey:sectionName];
    
    //NSLog(@"Loading cell at section %i row %i", indexPath.section, indexPath.row);
    UITableViewCell *cell = nil;
    if (indexPath.section == [_headings indexOfObject:FEATURES]) {
        static NSString *iconCellNib = @"IconBasedCell";
        
        IconBasedCell *iconCell = [self getCellWithNib:iconCellNib
                                               ofClass:[IconBasedCell class]
                                             tableView:tableView];

        iconCell.headingLabel.text = [_theoryHeadings objectAtIndex:indexPath.row];
        iconCell.subheadingLabel.text = [_theorySubHeadings objectAtIndex:indexPath.row];
        iconCell.iconImageView.image = [_theoryIcons objectAtIndex:indexPath.row];
        
        cell = iconCell;
    }
    else if (indexPath.section == [_headings indexOfObject:COLOR_MORE]) {
        static NSString *iconCellNib = @"IconBasedCell";
        
        IconBasedCell *iconCell = [self getCellWithNib:iconCellNib
                                               ofClass:[IconBasedCell class]
                                             tableView:tableView];
#if LITE
        iconCell.headingLabel.text = @"Thousands more colors";
        iconCell.subheadingLabel.text = @"Click here to get this and more";
#else
        iconCell.headingLabel.text = @"Favorite brand not here?";
        iconCell.subheadingLabel.text = @"Click here to let us know";
#endif
        iconCell.iconImageView.image = [UIImage imageNamed:@"Icon.png"];
        //iconCell.iconImageView.image = [UIImage imageNamed:@"Icon.png"];
        cell = iconCell;
    }
    else if (brand) {
        if (indexPath.section != _expandedIndex && indexPath.row == MAX_INITIAL_CATEGORIES) {
            static NSString *moreNib = @"MoreCell";
            MoreCell *moreCell = [self getCellWithNib:moreNib
                                              ofClass:[MoreCell class]
                                            tableView:tableView];
            
            cell = moreCell;
        } else {
            static NSString *categoryCellNib = @"ColorCategoryCell";
            ColorCategoryCell *categoryCell = [self getCellWithNib:categoryCellNib
                                                           ofClass:[ColorCategoryCell class]
                                                         tableView:tableView];
            
            categoryCell.colorCategory = [self colorCategoryForRow:indexPath.row brand:brand];
            cell = categoryCell;
        }
        
    }
    else {
        static NSString *genericCellIdentifier = @"GenericCell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:genericCellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:genericCellIdentifier];
        }
        
        //cell.backgroundColor = UICOLOR_SIDEBAR_ITEM;
        cell.textLabel.text = @"No info";
        cell.textLabel.textColor = UICOLOR_SIDEBAR_ITEM_TEXT;
    }
    
    
    cell.backgroundView.backgroundColor = UICOLOR_SIDEBAR_ITEM;
    
    return cell;
}

- (ColorCategory *)colorCategoryForRow:(int)row brand:(Brand *)brand
{
    int index = 0;
    for (ColorCategory *category in brand.categories) {
        if (index  == row) {
            return category;
        }
        index ++;
    }
    return nil;
}

- (id)getCellWithNib:(NSString *)nibName ofClass:(Class)aClass tableView:(UITableView *)tableView;
{
    id cell = [tableView dequeueReusableCellWithIdentifier:nibName];
    
    if (cell && [cell isKindOfClass:[aClass class]]) return cell;
    
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    
    for (id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:aClass])
        {
            return currentObject;
            break;
        }
    }
    return nil;
}

#pragma mark @protocol UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    UIImageView *bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"side_bar_heading.png"]];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, bgImage.frame.size.height)];
    [view addSubview:bgImage];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, bgImage.frame.size.height)];
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    [self setHeaderLabelStyle:label];
    [view addSubview:label];
    
    if (self.searchMode) {
        label.text = [self tableView:tableView titleForHeaderInSection:section];
        return view;
    }
    
    NSString *sectionName = [self tableView:self.tableView titleForHeaderInSection:section];
    Brand *brand = [_colorBrands objectForKey:sectionName];
    
    //arrow
    int itemCount = [self numberOfRowsInSection:section];
    if (brand && itemCount > MAX_INITIAL_CATEGORIES) {
        label.text = [NSString stringWithFormat:@"%@ (%i)", label.text, itemCount];
        
        UIImageView *arrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"side_bar_heading_arrow.png"]];
        arrowImage.frame = CGRectCenterY(arrowImage.frame, view.frame);
        arrowImage.frame = CGRectSetX(bgImage.frame.size.width - 30, arrowImage.frame);
        arrowImage.tag = section;
        
        //rotate arrow
        if (section == _expandedIndex) {
            CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI_2);
            arrowImage.transform = transform;
            _expandedArrow = arrowImage;
        }
        [view addSubview:arrowImage];
        
        UIButton *touchButton = [[UIButton alloc] initWithFrame:bgImage.frame];
        touchButton.tag = 100 * section;
        [touchButton addTarget:self action:@selector(toggleHeaderExpanded:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:touchButton];
        view.userInteractionEnabled = YES;
    }
    
    return view;
}

- (void)setHeaderLabelStyle:(UILabel *)label
{
    label.backgroundColor = [UIColor clearColor];
    label.opaque = NO;
    label.font = [UIFont boldSystemFontOfSize:11.0];
    label.textColor = UICOLOR_SIDEBAR_HEADER_TEXT;
    label.shadowOffset = CGSizeMake(1,1);
    label.shadowColor = UICOLOR_SIDEBAR_HEADER_TEXT_SHADOW;
}
         
- (void) toggleHeaderExpanded:(UIView *)touchButton;
{
    int section = [touchButton tag] / 100;
    
    [self toggleSection:section];
}

- (void) toggleSection:(int)section
{
    UIView *header = [self tableView:_tableView viewForHeaderInSection:section];
    
    UIView *arrowView = [header viewWithTag:section];
    
    if (_expandedArrow && _expandedIndex != section) {
        _expandedArrow.transform = CGAffineTransformMakeRotation(0);
    }
    
    CGAffineTransform transform;
    BOOL expand = NO;
    if (_expandedIndex == section) {
        //toggled back
        _expandedIndex = -1;
        _expandedArrow = nil;
        transform = CGAffineTransformMakeRotation(0);
    }
    else {
        expand = YES;
        _expandedIndex = section;
        _expandedArrow = arrowView;
        transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        arrowView.transform = transform;
    } completion:^(BOOL finished){
        [self.tableView reloadData];
        
        if (expand) {
            const NSUInteger indexArr[] = {section, 0};
            NSIndexPath *path = [NSIndexPath indexPathWithIndexes:indexArr length:2];
            [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *sectionName = [self tableView:tableView titleForHeaderInSection:indexPath.section];
    
    Brand *brand = [_colorBrands objectForKey:sectionName];
    
    if (brand && indexPath.section != _expandedIndex && indexPath.row == MAX_INITIAL_CATEGORIES) {
        return 30;
    }
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return 30;
}

- (NSIndexPath *)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.searchMode) {
        [self.searchText resignFirstResponder];
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(SideBar_PickedColor:)]) {
            Color *color = [_searchResults objectAtIndex:indexPath.row];
            [self.delegate SideBar_PickedColor:color];
        }
        return indexPath;
    }
    
    NSString *sectionName = [self tableView:tableView titleForHeaderInSection:indexPath.section];
    
    //Brand *brand = [_colorBrands objectForKey:sectionName];
    
    if ([sectionName isEqualToString:[FEATURES uppercaseString]]) {
        NSString *feature = [_theoryHeadings objectAtIndex:indexPath.row];
        
        if ([feature isEqualToString:FEATURES_LIBRARY]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(SideBar_ShowLibrary)]) {
                [self.delegate SideBar_ShowLibrary];
            }
        }
        else if ([feature isEqualToString:FEATURES_COLOR_PICKER]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(SideBar_OpenedColorPicker)]) {
                [self.delegate SideBar_OpenedColorPicker];
            }
        }
        else if ([feature isEqualToString:FEATURES_COLOR_CAMERA]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(SideBar_OpenedHarmony:)]) {
                [self.delegate SideBar_OpenedHarmony:YES];
            }
        }
        else if ([feature isEqualToString:FEATURES_COLOR_LIBRARY]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(SideBar_OpenedHarmony:)]) {
                [self.delegate SideBar_OpenedHarmony:NO];
            }
        }
        else if ([feature isEqualToString:FEATURES_COLOR_FAVORITES]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(SideBar_ShowFavorites)]) {
                [self.delegate SideBar_ShowFavorites];
            }
        }
        else if ([feature isEqualToString:FEATURES_EXAMPLE]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(SideBar_LoadImage:)]) {
                [self.delegate SideBar_LoadImage:[UIImage imageNamed:INITIAL_IMAGE]];
            }
        }
        else if ([feature isEqualToString:FEATURES_COMPLEX_EXAMPLE]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(SideBar_LoadImage:)]) {
                [self.delegate SideBar_LoadImage:[UIImage imageNamed:@"lattice.jpg"]];
            }
        }
        //SideBar_OpenedColorPicker
        
    }
    else if ([sectionName isEqualToString:[COLOR_MORE uppercaseString]]) {
#if LITE
        //open up app in itunes
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:PRO_URL]];
#else 
        [self.delegate SideBar_EmailMore];
#endif
    }
    else if ([cell isKindOfClass:[ColorCategoryCell class]]) {
        
        ColorCategory *category = [(ColorCategoryCell *)cell colorCategory];
        
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(SideBar_PickedColorCategory:)]) {
            [self.delegate SideBar_PickedColorCategory:category];
        }
    }
    else if ([cell isKindOfClass:[MoreCell class]]) {
        [self toggleSection:indexPath.section];
    }
    
    return indexPath;
}

- (void)viewDidUnload {
    [self setSearchText:nil];
    [self setTableView:nil];
    [super viewDidUnload];
}
@end
