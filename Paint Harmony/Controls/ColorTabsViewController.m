//
//  ColorTabsViewController.m
//  Home Painter
//
//  Created by Joel Teply on 1/21/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import "ColorTabsViewController.h"
#import "SwatchesViewController.h"
#import "Brand.h"
#import "SettingsModel.h"
#import "ColorScroller.h"
#import "Color+Extended.h"

#define SLIDE_ANIMATION_TIME 0.33

#define TAB_MIN_Y 40
#define TAB_Y_INSET -2

@interface ColorTabsViewController() <ColorScrollerDelegate> {
    BOOL _setupInterface;
    dispatch_queue_t _finderQueue;
}

@property (strong, nonatomic) SwatchesViewController *swatches;
@property (strong, nonatomic) ColorScroller *favorites;

@end

@implementation ColorTabsViewController

@synthesize delegate;
@synthesize hsvColorSelector;
@synthesize hsvColorButton;
@synthesize hueSlider, hueTextbox, satSlider, satTextbox, valSlider, valTextbox;

@synthesize swatchesSelector;
@synthesize swatchesButton;
@synthesize swatches;

@synthesize favoritesSelector;
@synthesize favoritesButton;

#pragma mark - View lifecycle

- (void)viewDidLoad;
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    self.view.opaque = NO;
    
    _finderQueue = dispatch_queue_create("ColorTabsFinder", NULL);
    
    self.swatches = [[SwatchesViewController alloc] initWithNibName:nil bundle:nil];
    self.swatches.delegate = self;
    
    [self configureCheckboxes];

    self.swatches.view.frame = CGRectSetOrigin(CGPointMake(0, 0), self.swatches.view.frame);
    [self.swatchesSelector addSubview:self.swatches.view];
    [self.swatches refresh];
    
    self.favorites = [[ColorScroller alloc] initWithDelegate:self
                                                       title:nil colors:nil texture:nil size:IS_WIDESCREEN ? CGSizeMake(100,100) : CGSizeMake(80,80)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favoritesChanged) name:FavoritesChangedNotification object:nil];
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if (_setupInterface) return;
    
    _setupInterface = YES;
    
    float totalW = IS_IPAD ? 768 : 320;
    
    self.drawerButton.frame = CGRectSetY(TAB_Y_INSET, self.drawerButton.frame);
    
    //color
    self.hsvColorSelector.frame = CGRectSetW(totalW, self.hsvColorSelector.frame);
    
    //Swatches
    self.swatchesSelector.frame = CGRectSetW(totalW, self.swatchesSelector.frame);
    
    self.swatches.view.frame = CGRectSetOrigin(CGPointMake(0, 0), self.swatches.view.frame);
    
    self.swatchesSelector.frame = CGRectSetH(self.swatches.view.frame.size.height, self.swatchesSelector.frame);
    self.swatches.view.frame = CGRectSetW(self.swatchesSelector.frame.size.width, self.swatches.view.frame);
    
    //favorites
    self.favoritesSelector.frame = CGRectSetSize(CGSizeMake(totalW, 120), self.favoritesSelector.frame);
    
    self.hsvColorButton.frame = CGRectSetY(CGRectGetMaxY(self.hsvColorSelector.frame) + TAB_Y_INSET, self.hsvColorButton.frame);
    
    self.swatchesButton.frame = CGRectSetY(CGRectGetMaxY(self.swatchesSelector.frame) + TAB_Y_INSET, self.swatchesButton.frame);

    
    [self.favoritesSelector addSubview:self.favorites.view];
    self.favoritesSelector.frame = CGRectSetH(self.favorites.view.frame.size.height, self.favoritesSelector.frame);    
    self.favoritesButton.frame = CGRectSetY(CGRectGetMaxY(self.favoritesSelector.frame) + TAB_Y_INSET, self.favoritesButton.frame);
    self.favorites.view.frame = CGRectSetW(self.view.frame.size.width, self.favorites.view.frame);
    
    [self toggleMenu:self.swatchesSelector button:self.swatchesButton open:NO resizeFrame:NO tab:ColorTabSwatches];
    [self toggleMenu:self.hsvColorSelector button:self.hsvColorButton open:NO resizeFrame:NO tab:ColorTabHSV];
    [self toggleMenu:self.favoritesSelector button:self.favoritesButton open:NO resizeFrame:NO tab:ColorTabFavorites];
    
    self.view.frame = CGRectSetW(self.view.superview.frame.size.width, self.view.frame);
    self.view.frame = CGRectSetH(self.favoritesButton.frame.size.height + TAB_Y_INSET, self.view.frame);
    
    [self favoritesChanged];
}

- (void)viewDidUnload
{
    [self setHueSlider:nil];
    [self setValSlider:nil];
    [self setSatSlider:nil];
    [self setHsvColorSelector:nil];
    [self setHueTextbox:nil];
    [self setSatTextbox:nil];
    [self setValTextbox:nil];
    [self setHsvColorButton:nil];

    [self setDrawerButton:nil];
    [self setMatchClosestCheckbox:nil];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self _colorChanged:NO];
}

- (void) SelectColor_PickedColor:(Color *)color uiColor:(UIColor*)uiColor;
{
    [self changeGlobalColor:color uiColor:uiColor findMatch:NO sender:nil];
}


- (void) changeGlobalColor:(Color *)color uiColor:(UIColor*)uiColor findMatch:(BOOL)findMatch sender:(id)sender;
{
    if (color) {
        [PainterUtility setCurrentColor:color uiColor:color.uiColor sender:sender];
    }
    else if (findMatch) {
        [self locateColorMatch:uiColor sender:sender];
    }
    else {
        [PainterUtility setCurrentColor:nil uiColor:uiColor sender:sender];
    }
}

- (void) locateColorMatch:(UIColor *)uiColor sender:(id)sender
{
    Brand *brand = self.brandCheckbox.enabled && self.brandCheckbox.selected
        ? self.swatches.colorCategory.brand : NULL;
    
    ColorCategory *category = self.categoryCheckbox.enabled && self.categoryCheckbox.selected
        ? self.swatches.colorCategory : NULL;

    [CBThreading performBlock:^{
        Color *newColor = [PainterUtility closestMatchForUIColor:uiColor brand:brand category:category];
        //NSLog(@"Closest color is %@", newColor.name);
        dispatch_async(dispatch_get_main_queue(), ^{
            [PainterUtility setCurrentColor:newColor uiColor:newColor.uiColor sender:sender];
        });
    } onQueue:_finderQueue
      withIdentifier:@"locateColorMatch"];
    
}

- (void) colorChangedNotification:(id)sender;
{
    if (sender != self) {
        [self _colorChanged:NO];
    }
}

- (void) _colorChanged:(BOOL)ignoreSliders;
{
    if (!ignoreSliders) {
        float hue, sat, val;
        [[PainterUtility currentUIColor] getHue:&hue saturation:&sat brightness:&val alpha:NULL];
        hueSlider.value = hue;
        satSlider.value = sat;
        valSlider.value = val;
        
        hueTextbox.text = [NSString stringWithFormat:@"%i", (int) (hue * 360.0f)];
        satTextbox.text = [NSString stringWithFormat:@"%i", (int) (sat * 255.0f)];
        valTextbox.text = [NSString stringWithFormat:@"%i", (int) (val * 255.0f)];
    }
}

- (IBAction)drawerClicked:(id)sender {
    if ([self.delegate respondsToSelector:@selector(drawerButtonClicked)]) {
         [self.delegate drawerButtonClicked];
    }
}

- (IBAction)colorChanged:(id)sender {
    float hue = hueSlider.value;
    float sat = satSlider.value;
    float val = valSlider.value;
    
    hueTextbox.text = [NSString stringWithFormat:@"%i", (int) (hue * 360.0f)];
    satTextbox.text = [NSString stringWithFormat:@"%i", (int) (sat * 255.0f)];
    valTextbox.text = [NSString stringWithFormat:@"%i", (int) (val * 255.0f)];
    
    [self _colorChanged:YES];
    
    UIColor *uiColor = [UIColor colorWithHue:hue saturation:sat brightness:val alpha:1.0];
    
    [self changeGlobalColor:NULL uiColor:uiColor findMatch:self.matchClosestCheckbox.selected sender:self];
    
}

- (void) setCurrentTab:(ColorTab)value {
    _currentTab = value;
    
    if (value == ColorTabNone) {
        [self closeAllTabs];
    } else {
        [self toggleTab:value open:YES];
    }
}

- (void) closeAllTabs;
{
    [self toggleTab:ColorTabHSV open:NO];
    [self toggleTab:ColorTabFavorites open:NO];
    [self toggleTab:ColorTabSwatches open:NO];
}

- (IBAction)hsvClicked {
    bool isOpen = self.hsvColorButton.frame.origin.y > TAB_MIN_Y;
    bool shouldOpen = !isOpen;
    
    if (shouldOpen) [self _colorChanged:NO];
    
    [self toggleTab:ColorTabHSV open:shouldOpen];
}

- (IBAction)swatchesClicked {
    bool isOpen = self.swatchesButton.frame.origin.y > TAB_MIN_Y;
    bool shouldOpen = !isOpen;

    [self toggleTab:ColorTabSwatches open:shouldOpen];
}

- (IBAction)favoritesClicked {
    self.favorites.view.hidden = ![self.favorites.colors count];
    self.favoritesInstructions.hidden = !self.favorites.view.hidden;
    
    bool isOpen = self.favoritesButton.frame.origin.y > TAB_MIN_Y;
    bool shouldOpen = !isOpen;
    
    [self toggleTab:ColorTabFavorites open:shouldOpen];
}

- (void) favoritesChanged
{
    [self refreshFavorites];
}

- (void) toggleTab:(ColorTab)tab open:(BOOL)open;
{
    switch (tab) {
        case ColorTabHSV:
            [self toggleMenu:self.hsvColorSelector button:self.hsvColorButton open:open resizeFrame:YES tab:tab];
            break;
        case ColorTabSwatches:
            [self toggleMenu:self.swatchesSelector button:self.swatchesButton open:open resizeFrame:YES tab:tab];
            break;
        case ColorTabFavorites:
            [self toggleMenu:self.favoritesSelector button:self.favoritesButton open:open resizeFrame:YES tab:tab];
            break;
        default:
            break;
    }
}

- (void) toggleMenu:(UIView *)menu
             button:(UIButton *)button
               open:(BOOL)open
        resizeFrame:(BOOL)resizeFrame
                tab:(ColorTab)tab;
{
    int openPos =  0;
    int closePos = openPos - menu.frame.size.height;
    
    BOOL isOpen = button.frame.origin.y > 0;
    
    if (isOpen == open) return;
    
    int buttonYAdd = button.frame.origin.y - menu.frame.origin.y;
    
//    NSLog(@"%@ open pos=%i, close pos=%i, buttonYAdd=%i",
//          button.titleLabel.text,
//          openPos, closePos, buttonYAdd);
    
    int maxY = TAB_Y_INSET + (open ? menu.frame.size.height + button.frame.size.height : button.frame.size.height);
    //NSLog(@"max=%i", maxY);
    //self.view.frame = CGRectSetH(maxY, self.view.frame);
    
    [UIView animateWithDuration:SLIDE_ANIMATION_TIME animations:^{
        if (open) {
            menu.frame = CGRectSetY(openPos, menu.frame);
        } else {
            menu.frame = CGRectSetY(closePos, menu.frame);
        }
        button.frame = CGRectSetY(menu.frame.origin.y + buttonYAdd, button.frame);
       // 
    } completion:^(BOOL finished) {
        if (resizeFrame) {
            self.view.frame = CGRectSetH(maxY, self.view.frame);
        }
        if (self.delegate) {
            if (open) {
                if ([self.delegate respondsToSelector:@selector(tabOpened:view:)]) {
                    [self.delegate tabOpened:tab view:menu];
                }
            } else {
                if ([self.delegate respondsToSelector:@selector(tabClosed:view:)]) {
                    [self.delegate tabClosed:tab view:menu];
                }
            }
        }
    }];
}

- (void)configureCheckboxes
{
    BOOL matchPickerColors = [[SettingsModel getInstance] matchPickerColors];
    self.matchClosestCheckbox.selected = matchPickerColors;
    
    BOOL canMatchBrand = [PainterUtility canMatchBrand:self.colorCategory.brand];
    [PainterUtility enableCheckbox:self.brandCheckbox enabled:matchPickerColors && canMatchBrand];
    self.brandCheckbox.selected = [[SettingsModel getInstance] matchBrand];
    self.brandCheckbox.hidden = !canMatchBrand;
    
    BOOL canMatchCategory = [PainterUtility canMatchCategory:self.colorCategory];
    [PainterUtility enableCheckbox:self.categoryCheckbox
                           enabled:self.brandCheckbox.enabled && self.brandCheckbox.selected && canMatchCategory];
    self.categoryCheckbox.selected = [[SettingsModel getInstance] matchCategory];
    self.categoryCheckbox.hidden = !canMatchCategory;
    
    if (IS_IPAD) {
        [self.brandCheckbox setTitle:self.colorCategory.brand.name forState:UIControlStateNormal];
        [self.categoryCheckbox setTitle:self.colorCategory.name forState:UIControlStateNormal];
    }
    
    [self.matchClosestCheckbox setTitle:self.brandCheckbox.enabled ? @"Match Products from" : @"Match Products"
                               forState:UIControlStateNormal];
}

- (IBAction)matchClosestCheckboxClicked:(id)sender;
{
    BOOL currentMatchPickerColors = [[SettingsModel getInstance] matchPickerColors];

    [[SettingsModel getInstance] setMatchPickerColors:!currentMatchPickerColors];
    [self configureCheckboxes];
    
    [self colorChanged:NO];
}

- (IBAction)brandCheckboxClicked:(id)sender;
{
    BOOL currentMatchBrand = [[SettingsModel getInstance] matchBrand];
    [[SettingsModel getInstance] setMatchBrand:!currentMatchBrand];
    
    [self configureCheckboxes];
}

- (IBAction)categoryCheckboxClicked:(id)sender;
{
    BOOL currentMatchCategory = [[SettingsModel getInstance] matchCategory];
    [[SettingsModel getInstance] setMatchCategory:!currentMatchCategory];
    
    [self configureCheckboxes];
}


- (ColorCategory *)colorCategory
{
    return self.swatches.colorCategory;
}

- (void)setColorCategory:(ColorCategory *)colorCategory;
{
    self.swatches.colorCategory = colorCategory;
    [self.swatchesButton setTitle:self.swatches.colorCategory.name forState:UIControlStateNormal];
    [self configureCheckboxes];
}

- (void)refreshFavorites
{
    __unsafe_unretained  ColorTabsViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.favorites.colors removeAllObjects];
        [weakSelf.favorites.colors addObjectsFromArray:[PainterUtility favorites]];
        [weakSelf.favorites refresh];
    });
}

- (void)ColorScroller_titleClicked:(ColorScroller *)colorScroller;
{
    
}

- (void)ColorScroller_colorClicked:(ColorScroller *)colorScroller color:(id)newColor;
{
    Color *color = newColor;
    [self changeGlobalColor:color uiColor:color.uiColor findMatch:NO sender:nil];
    [self toggleMenu:self.favoritesSelector button:self.favoritesButton open:NO resizeFrame:YES tab:ColorTabFavorites];
}

- (CGRect) closedFrame;
{
    return CGRectMake(self.view.frame.origin.x,
                      self.view.frame.origin.y,
                      self.view.frame.size.width,
                      self.swatchesButton.frame.size.height);
}

@end
