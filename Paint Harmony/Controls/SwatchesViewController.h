//
//  SwatchesViewController.h
//  Home Decorator
//
//  Created by Joel Teply on 1/3/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ColorCategory.h"
#import "Controls.h"

@class TapScrollView, Color;

@interface SwatchesViewController : BaseViewController <UIScrollViewDelegate>

@property (assign) id<SelectColorDelegate> delegate;
@property (weak, nonatomic) IBOutlet TapScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (strong, nonatomic) ColorCategory *colorCategory;
@property (strong, nonatomic) Color *selectedColor;

- (void) refresh;

@end
